void setup() { 
  size(768,576);
  colorMode(RGB,255);
  initSnakes();
  for (int i=0;i<10;i++) {
    snake[i].init();
    snake[i].start();
    snake[i].alive=true;
  }
}

void draw() { 
  background(0,0,0);
  for (int i=0;i<maxNrOfSnakes;i++) {
    if (snake[i].alive) {snake[i].update();}
  }
}



int maxNrOfSnakes=30;
Snake[] snake;

void initSnakes() {
  snake=new Snake[maxNrOfSnakes];
  for (int i=0;i<maxNrOfSnakes;i++) {
    snake[i]=new Snake();
  }
}

int findDeadSnake() {
  int i=-1;
  boolean found=false;
  while (i<maxNrOfSnakes-1&&!found) {
    i++;
    if (!snake[i].alive) {found=true;}
  }
  if (!found) {i=-1;}
  return i;
}

class Snake {
  float type=0;
  
  int maxLength=200, sLength=0;
  float lenPerc=0;
  
  float maxX=1.0, maxY=0.75, sScale=width;
  float[] x,y;  // positions 0..1
  
  float direction=0;
  
  boolean alive=false;
  
  int age=0, maxAge=300;
  float agePerc=0;
  
  Snake() { x=new float[maxLength]; y=new float[maxLength]; }
  
  void init() {  
    alive=false;
    sLength=0;
    age=0;
    type=random(5);
  }
  
  void start() {
    x[0]=random(maxX); y[0]=random(maxY);
    direction=random(TWO_PI);
  }
  void start(float _x, float _y) {
    x[0]=_x; y[0]=_y;
    direction=random(TWO_PI);
  }
  
  void update() { 
    if (age<maxAge) {
      age++;
      agePerc=age/(float)maxAge;
    } else {alive=false;}
    
    if (sLength<maxLength-1) {
      sLength++;
      lenPerc=sLength/(float)maxLength;
      direction+=0.5*(noise(x[sLength-1]*100,y[sLength-1]*100,(type+agePerc)*100)-0.5);
      float xInc=0.005*(1-lenPerc)*cos(direction);
      float yInc=0.005*(1-lenPerc)*sin(direction);
      x[sLength]=x[sLength-1]+xInc;
      y[sLength]=y[sLength-1]+yInc;
      
      if (red(get(int(x[sLength-1]*sScale),int(y[sLength-1]*sScale)))!=0) {
//      if (random(1)<0.003) {
        print("NEW SNAKE: ");
        int n=findDeadSnake(); println(n);
        if (n!=-1) {
          snake[n].init();
          snake[n].start(x[sLength-1],y[sLength-1]);
          snake[n].alive=true;      
        }
      }
    }
    
    
    
    for (int i=1;i<sLength;i++) {
      float c=i/(float)sLength*255*(1-agePerc);
      stroke(c);
      line(x[i]*sScale,y[i]*sScale,x[i-1]*sScale,y[i-1]*sScale);
    }
  } 
  
}
