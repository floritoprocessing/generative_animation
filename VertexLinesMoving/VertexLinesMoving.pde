
int amount = 5;
int steps = 19;
float[][] y = new float[amount][steps];
float[] ty = new float[amount];
float[] weight = new float[amount];

void setup() {
  size(800,300);
  for (int i=0;i<amount;i++) {
    weight[i] = random(1,5);
    if (i==0) {
      y[i][0] = height/2;
    } else {
      y[i][0] = y[i-1][0] + random(-30,30);
    }
    for (int xi=1;xi<y[i].length;xi++) {
      y[i][xi] = y[i][xi-1] + random(-20,20);
    }
  }
}

void draw() {
//  for (int i=0;i<amount;i++) {
//    y[i][0] += random(-15,15);
//  }

  float s = 0.98, is=1.0-s;
  for (int i=0;i<amount;i++) {
    for (int j=steps-1; j>0; j--) {
      y[i][j] = s*y[i][j] + is*y[i][j-1];
    }
  }
  
  background(50);
  smooth();
  stroke(255);
  noFill();
  
  for (int i=0;i<amount;i++) {
    int x = -50;
    strokeWeight(weight[i]);
    beginShape();
    for (int j=0;j<y[i].length;j++) {
      curveVertex(x,y[i][j]);
      x+=50;
    }
    endShape();
  }

}
