class Wavefront {

  // center position
  float x=0, y=0;
  
  // radius
  float r=0;
  
  // maxRadius
  float maxRadius=1;
  
  // amplitude of wavefront
  float amp=0;
  color col;

  Wavefront(float _x, float _y, float _maxR, float _amp) {
    x=_x;
    y=_y;
    maxRadius=_maxR;
    amp=_amp;
  }
  
  void update(WavefrontCreator wfc, float _add) {
    float circum=PI*r;
    float c=amp*(1-r/maxRadius);
    colorMode(RGB,1.0);
    col=color(c,c,c);
    for (float p=0;p<circum;p+=_add) {
      float rd=2*PI*p/circum;
      setAdd((int)(x+r*cos(rd)),(int)(y+r*sin(rd)),col);
    }
    
    if (r>maxRadius) {
      wfc.remove(this);
    }
    r+=_add;
    
  }
  
}

void setAdd(int x, int y, color col) {
  colorMode(RGB,1.0);
  color bg=get(x,y);
  float rr=constrain(red(bg)+red(col),0,1);
  float gg=constrain(green(bg)+green(col),0,1);
  float bb=constrain(blue(bg)+blue(col),0,1);
  set(x,y,color(rr,gg,bb));
}
