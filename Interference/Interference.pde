import java.util.Vector;

Wavefront wavefront;
WavefrontCreator wavefrontCreator1,wavefrontCreator2;

void setup() {
  size(640,480);
  wavefrontCreator1=new WavefrontCreator(280,200);
  wavefrontCreator2=new WavefrontCreator(360,280);
  
  wavefrontCreator1.setWavefrontMaxRad(150);
  wavefrontCreator2.setWavefrontMaxRad(150);
  
  wavefrontCreator1.setWavefrontMaxAmp(0.2);
  wavefrontCreator2.setWavefrontMaxAmp(0.2);
  
  wavefrontCreator1.setWaveFrontPixelIntensity(0.7);
  wavefrontCreator2.setWaveFrontPixelIntensity(0.7);
  
  wavefrontCreator1.setPhaseAdd(2*PI/10.0);
  wavefrontCreator2.setPhaseAdd(2*PI/10.0);
}

void draw() {
  background(0,0,0);
  noFill();
  wavefrontCreator1.update();
  wavefrontCreator2.update();
}
