class WavefrontCreator {

  // center of creator:
  float x=0;
  float y=0;


  // phase of creating waves:
  float pha=0;


  // maximum radius of wavefront:
  float maxRad=120;


  // maximum amplitude of wavefront:
  float maxAmp=0.2;


  // pixel intensity:
  float pixelIntens=0.5;

  
  // phase add value for every cycle
  float phaseAdd=2*PI/40.0;
  
  
  // Vector holding the wavefronts
  Vector wavefronts=new Vector();


  // CONSTRUCTOR:
  WavefrontCreator(float _x, float _y) {
    x=_x;
    y=_y;
  }

  
  void setPhaseAdd(float _pa) {
    phaseAdd=_pa;
  }
  
  
  
  
  // FUNCTIONS FOR SETTING WAVEFRONT VALUES:
  
  void setWavefrontMaxRad(float _maxRad) { 
    maxRad=_maxRad; 
  }

  void setWavefrontMaxAmp(float _maxAmp) { 
    maxAmp=_maxAmp; 
  }

  void setWaveFrontPixelIntensity(float _pi) {
    pixelIntens=_pi;
  }



  void update() {
    pha+=phaseAdd;
    if (pha>=2*PI) {
      pha-=2*PI;
    }
    float amp=maxAmp*(0.5+0.5*sin(pha));

    Wavefront tempWavefront=new Wavefront(x,y,maxRad,amp);
    wavefronts.add(tempWavefront);

    for (int i=0;i<wavefronts.size();i++) {
      tempWavefront=(Wavefront)wavefronts.elementAt(i);
      tempWavefront.update(this,pixelIntens);
    }
  }

  void remove(Wavefront wf) {
    wavefronts.remove(wf);
  }
}
