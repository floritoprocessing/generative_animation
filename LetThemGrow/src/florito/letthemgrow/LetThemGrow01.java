/**
 * 
 */
package florito.letthemgrow;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;

/**
 * @author Marcus
 *
 */
public class LetThemGrow01 extends PApplet {
	
	public static void main(String[] args) {
		PApplet.main(new String[] {"florito.letthemgrow.LetThemGrow01"});
	}
	
	ArrayList<Plant> plants = new ArrayList<Plant>();
	//Plant plant;
	PGraphics bgImage;
	
	public void settings() {
		size(1024,576,P3D);
	}
	
	public void setup() {
		
				
		bgImage = createGraphics(1024, 576, P3D);
		bgImage.beginDraw();
		bgImage.background(0);
		bgImage.beginShape(QUADS);
		bgImage.fill(128,128,255);
		bgImage.vertex(0,0);
		bgImage.fill(128,128,255);
		bgImage.vertex(bgImage.width,0);
		bgImage.fill(64,32,0);
		bgImage.vertex(bgImage.width,bgImage.height);
		bgImage.fill(64,32,0);
		bgImage.vertex(0,bgImage.height);
		bgImage.endShape();
		bgImage.endDraw();
		bgImage.updatePixels();
		addPlant();
	}
	
	private void addPlant() {
		plants.add(
				new Plant(
						this, 
						new PVector(random(-800,800),0,random(-800,800)), 
						random(15,35), 
						random(TWO_PI)));
	}
	
	public void draw() {
		background(0);//bgImage);
		smooth();
		if (Math.random()<0.1) {
			addPlant();
		}
		camera(0,-height*0.75f,500, 0,-height*0.5f,0, 0,1,0);
		
		rotateY(frameCount*0.001f);
		lights();
		
		for (Plant plant:plants) {
			plant.draw();
		}
		
		for (Plant plant:plants) {
			plant.grow();
		}
	}
}
