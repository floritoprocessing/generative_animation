/**
 * 
 */
package florito.letthemgrow;

import processing.core.PVector;

/**
 * @author Marcus
 * 
 */
public class PVectorMath {
	
	public static void rotY(PVector vec, float rd) {
		float SIN = (float)Math.sin(rd);
		float COS = (float)Math.cos(rd);
		float xn = vec.x * COS - vec.z * SIN;
		float zn = vec.z * COS + vec.x * SIN;
		vec.x = xn;
		vec.z = zn;
	}
}
