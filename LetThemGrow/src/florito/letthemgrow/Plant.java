/**
 * 
 */
package florito.letthemgrow;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;

/**
 * @author Marcus
 *
 */
public class Plant {
	
	private final PApplet pa;
	
	private final float GROW_SPEED = 2;
	private final float SEGMENT_SIZE = 20;
	
	private PVector base;
	private float baseWidth;
	private float orientation;
	//PLine line;
	private final ArrayList<PVectorLine> lines;
	private PVectorLine lastLine;
	
	public Plant(PApplet pa, PVector base, float baseWidth, float orientation) {
		this.pa = pa;
		this.base = base;
		this.baseWidth = baseWidth;
		this.orientation = orientation;
		
		
		lines = new ArrayList<PVectorLine>();
		lines.add(createSegmentLine(base, baseWidth, orientation));
		lastLine = createSegmentLine(base, baseWidth, orientation);
	}

	/**
	 * @param base
	 * @param baseWidth2
	 * @param orientation2
	 */
	private PVectorLine createSegmentLine(
			PVector base, 
			float baseWidth,
			float orientation) {
		
		PVector off0 = new PVector(baseWidth/2,0,0);
		PVector off1 = new PVector(-baseWidth/2,0,0);
		
		//TODO: rotate points!
		PVectorMath.rotY(off0, orientation);
		PVectorMath.rotY(off1, orientation);
		
		PVector point0 = PVector.add(base, off0);
		PVector point1 = PVector.add(base, off1);
		
		
		
		return new PVectorLine(point0, point1, orientation);
	}

	public void grow() {
		
		if (baseWidth<=0) {
			return;
		}
		
		PVector growVector = new PVector(0,-GROW_SPEED,0);
		base.add(growVector);
		
		orientation += (pa.noise(lines.get(0).p0.x, lines.get(0).p0.z, lastLine.p0.y*0.1f)-0.5f)*0.2f;
		
		//orientation += 0.02f;
		lastLine = createSegmentLine(base, baseWidth, orientation);
		
		if (lastLine.p0.y < lines.get(lines.size()-1).p0.y-SEGMENT_SIZE) {
			//System.out.println("DA");
			lines.add(lastLine);
			lastLine = createSegmentLine(base, baseWidth, orientation);
		}
		
		baseWidth -= 0.1f;
	}

	public void draw() {
		//pa.stroke(255,0,0);
		pa.noStroke();
		pa.fill(22,200,32);
		pa.beginShape(PApplet.QUAD_STRIP);
		for (PVectorLine pl:lines) {
			pa.vertex(pl.p0.x, pl.p0.y, pl.p0.z);
			pa.vertex(pl.p1.x, pl.p1.y, pl.p1.z);
		}
		pa.vertex(lastLine.p0.x, lastLine.p0.y, lastLine.p0.z);
		pa.vertex(lastLine.p1.x, lastLine.p1.y, lastLine.p1.z);
		pa.endShape();
	}
}
