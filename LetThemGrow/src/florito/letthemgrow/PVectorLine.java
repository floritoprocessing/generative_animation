/**
 * 
 */
package florito.letthemgrow;

import processing.core.PVector;

/**
 * @author Marcus
 *
 */
public class PVectorLine {
	
	final PVector p0, p1;
	final float orientation;

	public PVectorLine(PVector p0, PVector p1, float orientation) {
		this.p0 = p0;
		this.p1 = p1;
		this.orientation = orientation;
	}

	/*public PVector getPoint0() {
		return p0;
	}
	
	public PVector getPoint1() {
		return p1;
	}*/
	
}
