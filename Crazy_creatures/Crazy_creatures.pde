int nrOfCreatures=1500;
Creature[] creature=new Creature[nrOfCreatures+1];

PImage bg;

void setup() {
  size (400,300,P3D);
  bg=loadImage("Sides.jpg");
  createAllCreatures();
}

void draw() {
  image(bg,0,0);
  updateAllCreatures();
}


void createAllCreatures() {
  for (int i=1;i<=nrOfCreatures;i++) {
    creature[i]=new Creature();
  }
}

void updateAllCreatures() {
  for (int i=1;i<=nrOfCreatures;i++) {
    creature[i].update();
  }
}

class Creature {
  float happyness;  // -1 = totally unhappy; 1=totally happy
  float x,y;        // x/y position
  float xm,ym;      // x/y vector
  color col;        // color of creature;
  
  Creature() {
    happyness=random(-1,1);                      // create random happyness
    x=random(width); y=random(height);           // create random position
    xm=random(-.2,.2); ym=random(-.2,.2);      // create random movement vector
    col=bg.get(int(x),int(y));
  }
  
  void update() {
    x=inRangeX(x+xm); y=inRangeY(y+ym);
    plotCreature(x,y);
  }
  
  void plotCreature(float x, float y) {
    set(int(x),int(y),col);
  }
  
}

float inRangeX(float n) {
  while (n<1) {n+=width;} while (n>width) {n-=width;} return n;
}
float inRangeY(float n) {
  while (n<1) {n+=height;} while (n>height) {n-=height;} return n;
}
