/*

 The Years
 Song by I Love Dora
 
 Piano:
 With every Piano chord, there are roots growing on specified points on screen
 Pattern: 1.2.3.4... 1.2.3.4.5.
 
 Pling:
 
 Drill:
 
 Singing:
 
 
 
 */

boolean paused = false;
float mainScale=800;  // factor to screen

void setup() {
  size(800, 600); 
  colorMode(RGB, 255); 
  colorSetup(); 
  background(brown2); 
  smooth();
  initBranches();
}

void draw() {
  if (!paused) {
    background(brown2);
    updateBranches();
  }
}

void keyPressed() {
  if (key=='1') { 
    branch[0].activate(0.08);
  }
  if (key=='2') { 
    branch[1].activate(0.08);
  }
  if (key=='3') { 
    branch[2].activate(0.08);
  }
  if (key=='4') { 
    branch[3].activate(0.08);
  }
  if (key=='5') { 
    branch[4].activate(0.08);
  }
  if (key=='p')
    paused=!paused;
}




color brown1, brown2, green1;
void colorSetup() {
  brown1=color(51, 36, 9);
  brown2=color(42, 28, 4);
  green1=color(81, 221, 101);
}





Branch[] branch;
int nrOfBranches=5;

void initBranches() {
  branch=new Branch[nrOfBranches];
  for (int i=0; i<nrOfBranches; i++) { 
    branch[i]=new Branch();
  }
}
void updateBranches() { 
  for (int i=0; i<nrOfBranches; i++) { 
    branch[i].update();
  }
}

class Branch {
  float x=0, y=0;
  float len=0, tLen=0.05, cLen=0.05;
  float baseDir=0, SIN=0, COS=0;
  float trans=255;
  boolean active=false;

  int nrOfChildren=2;
  boolean children=false;
  Branch[] subBranch=new Branch[nrOfChildren];

  Branch() { 
    initPosition(); 
    initDirection();
  }

  Branch(float _x, float _y, float _baseDir) {
    x=_x; 
    y=_y;
    baseDir=_baseDir+random(-radians(90), radians(90)); 
    SIN=sin(baseDir); 
    COS=cos(baseDir);
  }

  void initPosition() { 
    x=random(.2, .8); 
    y=random(.15, .6);
  }
  void initDirection() { 
    baseDir=random(TWO_PI); 
    SIN=sin(baseDir); 
    COS=cos(baseDir);
  }

  void activate(float _tLen) { 
    active=true;
    children=false;
    len=0; 
    tLen=_tLen; 
    float degOffset=random(-radians(5), radians(5));
    SIN=sin(baseDir+degOffset); 
    COS=cos(baseDir+degOffset);
    trans=255;
  }

  void update() {
    if (active) {
      len+=tLen*0.3;
      if (len>tLen) {
        len=tLen;
      }
      trans=(7*trans)/8.0;
      if (len/tLen>0.98&&!children) {
        children=true;
        float cLen=tLen*0.85;
        if (cLen>0.02) {
          for (int i=0; i<nrOfChildren; i++) { 
            subBranch[i]=new Branch(x+len*COS, y+len*SIN, baseDir); 
            subBranch[i].activate(cLen);
          }
        } else {
          children=false;
        }
      }

      stroke(81, 221, 101, trans);
      screenLine(x, y, x+len*COS, y+len*SIN);

      if (children) {
        for (int i=0; i<nrOfChildren; i++) { 
          subBranch[i].update();
        }
      }
    }
  }
}




void screenLine(float x1, float y1, float x2, float y2) {
  line(x1*mainScale, y1*mainScale, x2*mainScale, y2*mainScale);
}
