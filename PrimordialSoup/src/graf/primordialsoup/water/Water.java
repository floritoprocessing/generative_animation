package graf.primordialsoup.water;

import graf.primordialsoup.PrimordialSoup;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PImage;

import traer.physics.Particle;
import traer.physics.ParticleSystem;
import traer.physics.Spring;
import traer.physics.Vector3D;

public class Water {

	/*
	 * Physics settings
	 */
	private static float gravityY = 0.0f;
	private static float drag = 0.02f;
	
	/*
	 * Particle settings
	 */
	private static float mass = 1f;
	
	/*
	 * Spring settings
	 */
	/**
	 * Strength, If they are strong they act like a stick. If they are weak they take a long time to return to their rest length. 
	 */
	private static float strength = 1.6f;  //8
	/**
	 * Damping, If springs have high damping they don't overshoot and they settle down quickly, with low damping springs oscillate. 
	 */
	private static float damping = 0.01f; // 0.5
	/**
	 * Rest Length, the spring wants to be at this length and acts on the particles to push or pull them exactly this far apart at all times. 
	 */
	private static float restLength;
	
	/*
	 * Interaction settings
	 */
	/**
	 * Positive strength is attraction negative strength is repulsion. 
	 */
	private static float interactionStrength = -5000;
	
	/**
	 * Get and set the minimum distance, which limits how strong the force can get close up. 
	 */
	private static float interactionMinimumDistance = 10;
	
	/*
	 * Physics
	 */
	private ParticleSystem physics;
	
	/*
	 * Interaction
	 */
	private boolean interactionEnabled = false;
	private boolean lastInteractionEnabled = false;
	private final float interactionRadius;
	private final float interactionRadiusSquared;
	
	/**
	 * position above the surface
	 */
	private final float interactionZPos;
	private Particle interactionParticle;
	//private static final Vector3D deadPosition = new Vector3D(-100,-100,-100);
	private Vector3D interactionPosition = new Vector3D();//deadPosition;//new Vector3D(-100,-100,-100);
	private Vector3D lastInteractionPosition = new Vector3D();
	
	private final int gridXAmount, gridYAmount;
	private final Particle[] gridParticleAtXY;
	
	private PImage texture = null;
	private final float[] textureU, textureV;
	
	public Water(int width, int height, float gridSize, float interactionRadius) {
		/*
		 * Create physics
		 */
		physics = new ParticleSystem(gravityY, drag);
		physics.setIntegrator(ParticleSystem.RUNGE_KUTTA);
		interactionParticle = physics.makeParticle();
		interactionZPos = interactionRadius/4;
		this.interactionRadius = interactionRadius;
		this.interactionRadiusSquared = interactionRadius * interactionRadius;
		
		/*
		 * Create particles
		 */
		gridXAmount = (int)(width/gridSize)+1;
		gridYAmount = (int)(height/gridSize)+1;
		gridParticleAtXY = new Particle[gridXAmount*gridYAmount];
		System.out.println("Creating grid of "+gridXAmount+" by "+gridYAmount+" particles");
		
		float z = 0;
		int i=0;
		for (int yi=0;yi<gridYAmount;yi++) {
			float y = yi*gridSize;
			for (int xi=0;xi<gridXAmount;xi++) {
				float x = xi*gridSize;
				Particle p = physics.makeParticle(mass,x,y,z);
				if (yi==0||xi==0||yi==gridYAmount-1||xi==gridXAmount-1) {
					p.makeFixed();
				} else {
					p.velocity().add((float)(Math.random()-0.5)*gridSize*0.1f, (float)(Math.random()-0.5)*gridSize*0.1f, 0);
//					p.addVelocity((float)(Math.random()-0.5)*gridSize*0.1f, (float)(Math.random()-0.5)*gridSize*0.1f, 0);
				}
				gridParticleAtXY[i++] = p; 
			}
		}
		System.out.println(i+" particles created");
		
		/*
		 * Create springs
		 */
		restLength = gridSize;
		i=0;
		for (int yi=0;yi<gridYAmount;yi++) {
			for (int xi=0;xi<gridXAmount;xi++) {
				if (xi<gridXAmount-1) {
					physics.makeSpring(gridParticleAtXY[i], gridParticleAtXY[i+1], strength, damping, restLength);
				}
				if (yi<gridYAmount-1) {
					physics.makeSpring(gridParticleAtXY[i], gridParticleAtXY[i+gridXAmount], strength, damping, restLength);
				}
				i++;
			}
		}
		
		/*
		 * Create texture points
		 */
		i=0;
		textureU = new float[gridXAmount*gridYAmount];
		textureV = new float[gridXAmount*gridYAmount];
		for (int y=0;y<gridYAmount;y++) {
			float yp = ((float)y/(gridYAmount-1));
			for (int x=0;x<gridXAmount;x++) {
				float xp = ((float)x/(gridXAmount-1));
				textureU[i] = xp;
				textureV[i] = yp;
				i++;
			}
		}
	}
	
	public void setInteractionEnabled(boolean interactionEnabled) {
		lastInteractionEnabled = this.interactionEnabled;
		this.interactionEnabled = interactionEnabled;
		
		//if (!interactionEnabled) lastInteractionPosition = deadPosition;
	}
	
	public void setInteractionPosition(int x, int y) {
		interactionPosition.set(x, y, 0);
	}

	public int getParticleAmount() {
		return physics.numberOfParticles();
	}
	
	public Particle getParticle(int index) {
		return physics.getParticle(index);
	}
	
	public int getSpringAmount() {
		return physics.numberOfSprings();
	}
	
	public Spring getSpring(int index) {
		return physics.getSpring(index);
	}
	
	//TODO: modified
	public Particle getLastInteractionParticle() {
//		if (!interactionParticle.isDead())
//			return interactionParticle;
//		else
			return null;
	}

	
	
	public void tick() {
		
//		interactionParticle.kill();
		
		if (interactionEnabled) {
			if (//lastInteractionPosition==null
					!lastInteractionEnabled
					|| lastInteractionPosition.x() != interactionPosition.x()
					|| lastInteractionPosition.y() != interactionPosition.y()) {
								
				//System.out.println("CLICK "+interactionPosition.x()+"/"+interactionPosition.y());
				ArrayList<Particle> inRange = getInteractionParticle2dNeighbours();
				
				// get average z-pos of grid
				float avZ = 0;
				int i=0;
				for (Particle p:inRange) {
					avZ += p.position().z();
					i++;
				}
				avZ /= i;
				//System.out.println(inRange.size());
				
				// make interaction particle
				interactionParticle = physics.makeParticle(
						1,
						interactionPosition.x(),
						interactionPosition.y(),
						avZ+interactionZPos);//interactionPosition.z());
				interactionParticle.makeFixed();
				
				// attract!
				for (Particle p:inRange) {
					physics.makeAttraction(interactionParticle, p, interactionStrength, interactionMinimumDistance);
				}
			}
			lastInteractionPosition = new Vector3D(interactionParticle.position());
		}
//		physics.advanceTime(1.0f);
		physics.tick(1);
	}

	private ArrayList<Particle> get2dNeighbours(float x, float y) {
		ArrayList<Particle> inRange = new ArrayList<Particle>();
		for (int i=0;i<physics.numberOfParticles();i++) {
			Particle p = physics.getParticle(i);
			float px = p.position().x();
			float py = p.position().y();
			float dx = x-px, dy = y-py;
			if (Math.abs(dx)<interactionRadius && Math.abs(dy)<interactionRadius) {
				if (dx*dx+dy*dy<interactionRadiusSquared) {
					if (x!=px || y!=py) {
						inRange.add(p);
					}
				}
			}
		}
		return inRange;
	}

	
	private ArrayList<Particle> getInteractionParticle2dNeighbours() {
		float mx = interactionParticle.position().x();
		float my = interactionParticle.position().y();
		return get2dNeighbours(mx, my);
	}
	


	public void drawAsTexture(PApplet pa, PImage img) {
		if (img!=texture) {
			texture = img;
		}
		//pa.stroke(255,0,0);
		pa.noStroke();
		pa.fill(0);
		pa.textureMode(PApplet.NORMAL);
		pa.beginShape(PApplet.QUADS);
		pa.texture(img);
		int i=0, ix;
		float xx, yy, zz;
		for (int y=0;y<gridYAmount-1;y++) {
			i=y*gridXAmount;
			for (int x=0;x<gridXAmount-1;x++) {
				ix=i;
				xx = gridParticleAtXY[ix].position().x();
				yy = gridParticleAtXY[ix].position().y();
				zz = gridParticleAtXY[ix].position().z();
				pa.vertex(xx,yy,zz,textureU[ix],textureV[ix]);
				ix=i+1;
				xx = gridParticleAtXY[ix].position().x();
				yy = gridParticleAtXY[ix].position().y();
				zz = gridParticleAtXY[ix].position().z();
				pa.vertex(xx,yy,zz,textureU[ix],textureV[ix]);
				ix=i+gridXAmount+1;
				xx = gridParticleAtXY[ix].position().x();
				yy = gridParticleAtXY[ix].position().y();
				zz = gridParticleAtXY[ix].position().z();
				pa.vertex(xx,yy,zz,textureU[ix],textureV[ix]);
				ix=i+gridXAmount;
				xx = gridParticleAtXY[ix].position().x();
				yy = gridParticleAtXY[ix].position().y();
				zz = gridParticleAtXY[ix].position().z();
				pa.vertex(xx,yy,zz,textureU[ix],textureV[ix]);
				i++;
			}
		}
		pa.endShape();
	}

	

	

}
