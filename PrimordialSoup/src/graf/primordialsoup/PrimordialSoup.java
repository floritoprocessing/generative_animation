package graf.primordialsoup;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import graf.primordialsoup.water.Water;
import processing.core.PApplet;
import processing.core.PImage;
import traer.physics.Particle;

public class PrimordialSoup extends PApplet {

	private static final long serialVersionUID = 2183137094771707483L;
	
	private int WIDTH = 800; // 1024
	private int HEIGHT = 600; // 768
	private float GRIDSIZE = 12.5f; //16
	private float MOUSE_INFLUENCE = 40;
	
	private float PARTICLE_DRAW_SIZE = 5;
	private float MOUSE_DRAW_SIZE = MOUSE_INFLUENCE*2;
	
	private Water water;
	private PImage texture = null;
	
	private enum DrawingMode {
		LINES (0),
		FILLS (1);
		
		private int index;
		DrawingMode(int index) {
			this.index = index;
		}
		
		public DrawingMode next() {
			index++;
			if (index==values().length) index=0;
			return DrawingMode.values()[index];
		}
	}
	
	private DrawingMode drawingMode = DrawingMode.LINES;
	
	
	/* (non-Javadoc)
	 * @see processing.core.PApplet#settings()
	 */
	@Override
	public void settings() {
		size(WIDTH, HEIGHT, P3D);
	}
	/*
	 * (non-Javadoc)
	 * @see processing.core.PApplet#setup()
	 */
	public void setup() {
		
		smooth();
		//hint(ENABLE_OPENGL_4X_SMOOTH);
		water = new Water(WIDTH,HEIGHT,GRIDSIZE,MOUSE_INFLUENCE);
		//frameRate(3);
		URL url = this.getClass().getResource("images/blue_water1024.jpg");
		System.out.println(url);
		
		try {
			File f = new File(url.toURI());
			texture = loadImage(f.getAbsolutePath());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void keyPressed() {
		if (key=='s') {
			saveFrame("output/PrimordialSoup_###.jpg");
		}
	}
	public void mousePressed() {
		if (mouseButton==RIGHT) {
			drawingMode = drawingMode.next();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see processing.core.PApplet#draw()
	 */
	public void draw() {
		
		water.setInteractionEnabled(mousePressed);		
		water.setInteractionPosition(mouseX,mouseY);
		
		water.tick();
		
		background(255);
		
		
		//int offset = 20;
		//camera(width/2 + offset,height/2,500,width/2 + offset,height/2,0,0,1,0);
		//rotateX(radians(15));
		
		
		if (drawingMode==DrawingMode.LINES) {
			drawSprings(0);
		} else if (drawingMode==DrawingMode.FILLS) {
			//directionalLight(255, 255, 255, 0, 0, -1);
			//pointLight(32, 32, 32, 0, 0, 1500);
			water.drawAsTexture(this,texture);
		}
		
		//water.drawInteractionLine(this);
		//drawParticles(color(255,0,0,128));
		
		//camera(width/2 - offset,height/2,500,width/2 - offset,height/2,0,0,1,0);
		//rotateX(radians(15));
		//drawSprings(color(0,255,0,128));
		//drawParticles(color(0,255,0,128));
		
		//drawSphere(color(0,0,255));
		
		
		
	}
	
	private void drawSprings(int color) {
		stroke(color);
		for (int i=0;i<water.getSpringAmount();i++) {
			float x0 = water.getSpring(i).getOneEnd().position().x();
			float y0 = water.getSpring(i).getOneEnd().position().y();
			float z0 = water.getSpring(i).getOneEnd().position().z();
			float x1 = water.getSpring(i).getTheOtherEnd().position().x();
			float y1 = water.getSpring(i).getTheOtherEnd().position().y();
			float z1 = water.getSpring(i).getTheOtherEnd().position().z();
			line(x0,y0,z0,x1,y1,z1);
		}
	}
	
	private void drawParticles(int color) {
		rectMode(CORNER);
		noFill();
		stroke(color);
		for (int i=0;i<water.getParticleAmount();i++) {
			float x = water.getParticle(i).position().x();
			float y = water.getParticle(i).position().y();
			float z = water.getParticle(i).position().z();
			pushMatrix();
			translate(0,0,z);
			rect(x-PARTICLE_DRAW_SIZE/2,y-PARTICLE_DRAW_SIZE/2,PARTICLE_DRAW_SIZE,PARTICLE_DRAW_SIZE);
			popMatrix();
		}
	}
	
	private void drawSphere(int color) {
		Particle m = water.getLastInteractionParticle();
		if (m!=null) {
			stroke(color);
			ellipseMode(CENTER);
			translate(m.position().x(),m.position().y(),m.position().z());
			sphere(MOUSE_DRAW_SIZE/2);
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.primordialsoup.PrimordialSoup"});
	}

}
