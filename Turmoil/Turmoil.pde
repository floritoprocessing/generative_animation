// W:Mute 2004
// www.wmute.org

// Variation on "Aquarium" by Ethan Miller (http://dma.sjsu.edu/~emiller/)
// http://dma.sjsu.edu/~emiller/projects/p5/self_organize1/applet/index.html

cell[] cells;
float cell_size = 10;
float min_spacing = cell_size + 2;
int partner_count = 1;
float partner_space = 0;
int d=400;
int w=400;
int h=400;
int a;
PImage bkg;





void setup(){
  size(500, 500, P3D);
  ellipseMode(DIAMETER);
  noStroke();
  //framerate(30);
  colorMode(HSB);

  if(partner_space < min_spacing) partner_space = min_spacing;
  cells = new cell[150];
  set_cells();
}

void set_cells(){
  for(int i = 0; i < cells.length; i++){
    cells[i] = new cell( random(-5,5), random(-5,5), random(-5,5),random(0.25,1.5)*cell_size);
  }
}

void draw(){
  background(30);
  int overspeed=0;
  translate(width/2,height/2,0);
  for(int c=0;c<10;c++){
    a=c*15;
    for(int i = 0; i < cells.length; i++){
      stroke(122, 51, 255,a/4);
      for(int j = i+1; j < cells.length; j++){
        float d = max(dist(cells[i].x, cells[i].y, cells[i].z,cells[j].x, cells[j].y,cells[j].z),0.0001);
        
        if(d<min_spacing){
          line(cells[i].x,cells[i].y,cells[i].z,cells[j].x,cells[j].y,cells[j].z);
        }
        if(d < min_spacing){
          // keep them apart
          float tot_rad=cells[i].s+cells[j].s;
          float tx = cells[j].x + (cells[i].x-cells[j].x)*tot_rad/d;
          float ty = cells[j].y + (cells[i].y-cells[j].y)*tot_rad/d;
          float tz = cells[j].z + (cells[i].z-cells[j].z)*tot_rad/d;
          float ax = (tx - cells[i].x) * 0.1;
          float ay = (ty - cells[i].y) * 0.1;
          float az = (tz - cells[i].z) * 0.1;
          cells[i].xt += ax;
          cells[i].yt += ay;
          cells[i].zt += az;
          cells[j].xt -= ax;
          cells[j].yt -= ay;
          cells[i].zt += az;
        }
      }
      stroke(122,200,255,a/2);
      cells[i].update();
      noStroke();
      cells[i].render();
      if (cells[i].overspeed()) overspeed++;
    }
    if (overspeed>100) set_cells();
  }
}

class cell{
  float s; //size
  float x; //actual x
  float xt; //target x
  float y; //actual y
  float yt; //target y
  float z; //actual z
  float zt; //target z
  float speed;

  float del; //delay
  int[] relations;
  public cell(float xpos, float ypos, float zpos, float sz){
    x = xpos;
    xt = xpos;
    y = ypos;
    yt = ypos;
    z=zpos;
    zt=zpos;
    s = sz;
    speed=0;
    del = 10 + random(3);
    relations = new int[partner_count];
    for(int ct = 0; ct < partner_count; ct++){
      int indx = int(random(cells.length));
      relations[ct] = indx;
    }
  }
  public void update(){
    walls();
    coord();
    float ox=x;
    float oy=y;
    float oz=z;
    float diff=0;
    if(x != xt){
      diff = xt - x;
      if(abs(diff) < 0.5) x = xt;
      else x += diff/del;
    }
    speed=diff*diff;
    if(y != yt){
      diff = yt - y;
      if(abs(diff) < 0.5) y = yt;
      else y += diff/del;
    }
    speed+=diff*diff;
    if(z != zt){
      diff = zt - z;
      if(abs(diff) < 0.5) z = zt;
      else z += diff/del;
    }
    speed+=diff*diff;
    speed=sqrt(speed)/del;
    if (speed<5)  line(ox,oy,oz,x,y,z);
  }
  public boolean overspeed(){
    return speed>15;
  }

  public void render(){
    pushMatrix();
    translate(screenX(x,y,z),screenY(x,y,z),z);
    fill(142+15*speed, 255, 185,(1-speed/15)*a);
    ellipse(-width/2,-height/2, s/150*(a+15), s/150*(a+15));
    popMatrix();
    
  }

  private void coord(){
    for(int p = 0; p < relations.length; p++){
      cell o = cells[relations[p]];
      float d = dist(x, y, z,o.x, o.y,o.z);
      if(d != partner_space){
        float xdiff = x - o.x;
        float ydiff = y - o.y;
        float zdiff = z - o.z;
        if(d < partner_space){
          xt += xdiff/del;
          yt += ydiff/del;
          zt += zdiff/del;
        }else{
          xt -= xdiff/del;
          yt -= ydiff/del;
          zt -= zdiff/del;
        }
      }
    }
  }

  private void walls(){
    if(x - (s/2) < -w/2) xt += w/2;
    if(x + (s/2) > w/2) xt -= w/2;
    if(y - (s/2) < -h/2) yt += h/2;
    if(y + (s/2) > h/2) yt -= h/2;
    if(z - (s/2) <  -d/2) zt += d/2;
    if(z + (s/2) >  d/2) zt -= d/2;

  }
}

void mouseReleased(){
  set_cells();
}
