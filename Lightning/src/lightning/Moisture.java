package lightning;

import processing.core.PApplet;
import processing.core.PImage;

public class Moisture extends PImage {

	private final int[] moisture;
	
	public Moisture(PApplet pa, int width, int height, float noiseFac) {
		super(width,height);
		
		moisture = new int[width*height];
		float seed = (float)Math.random();
		loadPixels();
		int i=0, x, y, c;
		float px, py;
		for (y=0;y<height;y++) {
			py = (float)y/(float)height*noiseFac;
			for (x=0;x<width;x++) {
				px = (float)x/(float)width*noiseFac;
				c = (int)(255*pa.noise(px, py, seed));
				pixels[i] = 0xff<<24 | c<<16 | c<<8 | c;
				moisture[i] = c;
				i++;
			}
		}
		updatePixels();
	}
	
	public int moistureAt(int x, int y) {
		if (x>=0&&x<width&&y>=0&&y<height) {
			return moisture[y*width+x];
		} else {
			return 0;
		}
	}
	
}
