package lightning;

public class PointCollection {

	private int INCREMENT = 100;
	private int size = 0;
	
	private int maxSize = 10;
	int[] opacity = new int[maxSize];
	float[] x = new float[maxSize];
	float[] y = new float[maxSize];
	
	public void addLine(float x0, float y0, int opacity0, float x1, float y1, int opacity1) {
		if (size>=maxSize) {
			growArray();
		}
		
		x[size] = x0;
		y[size] = y0;
		opacity[size] = opacity0;
		size++;
		x[size] = x1;
		y[size] = y1;
		opacity[size] = opacity1;
		size++;
	}
	
	public int getSize() {
		return size;
	}
	
	private void growArray() {
		float[] newX = new float[maxSize+INCREMENT];
		float[] newY = new float[maxSize+INCREMENT];
		int[] newO = new int[maxSize+INCREMENT];
		System.arraycopy(x, 0, newX, 0, maxSize);
		System.arraycopy(y, 0, newY, 0, maxSize);
		System.arraycopy(opacity, 0, newO, 0, maxSize);
		maxSize += INCREMENT;
		x = newX;
		y = newY;
		opacity = newO;
	}
	
}
