package lightning;

import java.util.Vector;

import processing.core.PApplet;
import processing.core.PGraphics;

public class PLightning extends PApplet {

	private static final long serialVersionUID = 6978738553645722945L;

	Moisture moisture;
	Vector<LightParticle> lightParticles;
	PointCollection lines;
	
	//PGraphics drawing;
	
	public void settings() {
		size(1024,768,P3D);
	}
	
	public void setup() {
		
		lightParticles = new Vector<LightParticle>();
		lines = new PointCollection();
		/*drawing = createGraphics(width, height, P3D);
		drawing.beginDraw();
		drawing.background(0,0,0,0);
		drawing.smooth();
		drawing.endDraw();*/
		moisture = new Moisture(this, width, height, 2f);
	}
	
	public void draw() {
		background(10,10,15);
		
		if (Math.random()<0.01f) {
			mouseButton=LEFT;
			mousePressed();
		}
		
		translate(width/2,0);
		rotateY(frameCount*0.005f);
		//translate(width/2,0);
		
		//image(moisture,0,0);
		
		// move and create new children
		Vector<LightParticle> newParticles = new Vector<LightParticle>();
		LightParticle newParticle;
		for (LightParticle lp:lightParticles) {
			newParticle = lp.move(moisture, 2.0f);
			if (newParticle!=null) newParticles.add(newParticle);
		}

		// add children
		lightParticles.addAll(newParticles);

		// remove all out of bounds
		// remove all low charges
		for (int i=lightParticles.size()-1;i>=0;i--) {
			LightParticle lp = lightParticles.get(i);
			if (lp.x<-width||lp.x>width||lp.y<0||lp.y>height-1||
					lp.charge<0.002f) {
				lightParticles.remove(i);
			}
		}
		
		// create lines
		float x0, y0, x1, y1;
		int o0, o1;
		for (LightParticle lp:lightParticles) {
			o0 = (int)map(lp.lcharge,0,1,10,200);
			o1 = (int)map(lp.charge,0,1,10,200);
			lines.addLine(lp.lx, lp.ly, o0, lp.x, lp.y, o1);
		}
		
		// draw all lines
		strokeWeight(1);
		beginShape(LINES);
		for (int i=0;i<lines.getSize();i++) {
			stroke(255,lines.opacity[i]);
			vertex(lines.x[i], lines.y[i]);
		}
		endShape();
		
		// draw all current particles
		// draw particles on canvas
		/*drawing.beginDraw();
		drawing.beginShape(LINES);
		for (LightParticle lp:lightParticles) {
			drawing.stroke(255,lp.charge*255);
			drawing.vertex(lp.lx, lp.ly);
			drawing.vertex(lp.x, lp.y);
		}
		drawing.endShape();
		drawing.endDraw();*/
		
		// draw canvas
		//image(drawing,0,0);
		
		// draw all particles
		
		beginShape(LINES);
		for (LightParticle lp:lightParticles) {
			stroke(255,lp.lcharge*255);
			vertex(lp.lx, lp.ly);
			stroke(255);
			vertex(lp.x, lp.y);
		}
		endShape();
		
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT) {
			lightParticles.add(
					new LightParticle(random(width*-0.03f,width*0.03f),0,
							radians(random(60,120))
							)
			);
		} else if (mouseButton==RIGHT) {
			setup();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"lightning.PLightning"});
	}

}
