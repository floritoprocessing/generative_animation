package lightning;

public class LightParticle {

	float lx, ly;
	float x, y;
	float mx, my;
	float rd;
	float lcharge = 1;
	float charge = 1;
	public static float drd = 0.08f;
	
	public LightParticle(float x, float y, float rd) {
		this.x=lx=x;
		this.y=ly=y;
		this.rd=rd;
		setDirVectors();
	}
	
	private static float[] xyFromDir(float rd) {
		return new float[] { (float)Math.cos(rd), (float)Math.sin(rd) };
	}
	
	private void setDirVectors() {
		mx = (float)Math.cos(rd);
		my = (float)Math.sin(rd);
	}
	
	public LightParticle move(Moisture moisture, float tick) {
		lx=x;
		ly=y;
		lcharge = charge;
		
		// change direction
		tick = Math.abs(tick);
		if (tick<1.0) {
			rd += tick*random(-drd,drd);
		} else {
			int t = (int)tick;
			for (int i=0;i<t;i++) {
				rd += random(-drd,drd);
			}
			rd += (tick-t)*random(-drd,drd);
		}
		setDirVectors();
		
		// calc future pos
		float fx = x + mx*tick;
		float fy = y + my*tick;
		x = fx;
		y = fy;
		
		//change charge
		charge -= tick*0.001f;
		charge = Math.max(0, charge);
		
		// offspring?
		//if (Math.pow(charge,10) * Math.random()<moisture.moistureAt((int)x, (int)y)*tick/255.0f * 0.01f) {
		if (Math.log10(charge*20) * Math.random() < moisture.moistureAt((int)x,(int)y)*tick/255.0f*0.050f) {
			float[] rd = new float[] {
					random(
					this.rd - (float)(Math.PI/2.0),
					this.rd + (float)(Math.PI/2.0)
					),
					random(
					this.rd - (float)(Math.PI/2.0),
					this.rd + (float)(Math.PI/2.0)
					),
			};
			float[] xy0 = xyFromDir(rd[0]);
			float[] xy1 = xyFromDir(rd[1]);
			int indexMorePointingDown = xy0[1]>xy1[1] ? 0 : 1;
			LightParticle child = new LightParticle(x, y, rd[indexMorePointingDown]);
			float chargeDistribution = random(0.2f,0.8f);
			child.charge = charge*chargeDistribution;
			charge = charge*(1-chargeDistribution);
			return child;
			
		} else {
			return null;
		}
	}
	
	private static float random(float v0, float v1) {
		return (float)(v0+Math.random()*(v1-v0));
	}

}
