class FloatyPoint {
  
  float alpha = 255;
  boolean alive = true;
  float x, y;
  float mx, my;
  
  FloatyPoint(Point pos, float mx, float my) {
    x = pos.x;
    y = pos.y;
    this.mx = mx;
    this.my = my;
  }
  
  public void draw() {
    if (alive) {
      stroke(255,alpha);
      x += mx;
      y += my;
      mx *= 0.97;
      my *= 0.97;
      alpha *= 0.97;
      float size = map(alpha,255,0,1,10);
      ellipse(x,y,size,size);
      if (alpha<1) {
        alive = false;
      }
    }
  }
  
}
