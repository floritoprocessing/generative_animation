class FloatyLine {
  
  FloatyPoint p0, p1;
  
  FloatyLine(FloatyPoint p0, FloatyPoint p1) {
    this.p0 = p0;
    this.p1 = p1;
  }
  
  void draw() {
    stroke(255,64);
    //line(p0.x,p0.y,p1.x,p1.y);
    beginShape(LINES);
    stroke(255,p0.alpha);
    vertex(p0.x,p0.y);
    stroke(255,p1.alpha);
    vertex(p1.x,p1.y);
    endShape();
  }
  
}
