Point getBestPointAround(ArrayList points, Point point) {
  
  if (point==null) {
    return null;
  }
  
  ArrayList foundPoints = new ArrayList();
  ArrayList differences = new ArrayList();
  for (int xo=-1;xo<=1;xo++) {
    for (int yo=-1;yo<=1;yo++) {
      if (xo!=0||yo!=0) {
       
        int x = point.x + xo;
        int y = point.y + yo;
        if (x>=0&&x<img.width&&y>=0&&y<img.height) {
          
          println(x+"x"+y);
          Point p = new Point(x,y);
          int diff = colorDiff(x,y);
          if (!points.contains(p) && diff<=tolerance) {
            foundPoints.add(p);
            differences.add(diff);
          }
          
        }
        
      }
    }
  }
  
  if (foundPoints.size()==0) {
    return null;
  } else {
    
    println("found "+foundPoints.size());
    int leastDiff = 3*255;
    int leastIndex = -1;
    for (int i=0;i<differences.size();i++) {
      int diff = (Integer)differences.get(i);
      if (diff<leastDiff) {
        leastDiff = diff;
        leastIndex = i;
      }
    }
    println(foundPoints.get(leastIndex));
    
    return (Point)foundPoints.get(leastIndex);
    
  }
  
}
