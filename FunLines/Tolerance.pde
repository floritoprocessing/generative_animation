boolean isLegalImagePoint(int x, int y) {
  int diff = colorDiff(x,y);
  if (diff<=tolerance) {
    return true;
  } else {
    return false;
  }
}

int colorDiff(int x, int y) {
  int c = img.get(x,y);
  int a = c>>24&0xff;
  if (a==255) {
    int r = c>>16&0xff;
    int g = c>>8&0xff;
    int b = c&0xff;
    return Math.abs(findR-r) + Math.abs(findG-g) + Math.abs(findB-b);
  }
  return 3*255;
  //return false;
}
