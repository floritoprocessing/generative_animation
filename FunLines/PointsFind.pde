ArrayList getBestPointsAround(ArrayList points, ArrayList srcPoints) {
  
  if (srcPoints==null || srcPoints.size()==0) {
    return new ArrayList();
  }
  
  ArrayList foundPoints = new ArrayList();
  
  for (int i=0;i<srcPoints.size();i++) {
  
    Point srcPoint = (Point)srcPoints.get(i);
    
    for (int xo=-1;xo<=1;xo++) {
      for (int yo=-1;yo<=1;yo++) {
        if (xo!=0||yo!=0) {
         
          int x = srcPoint.x + xo;
          int y = srcPoint.y + yo;
          if (x>=0&&x<img.width&&y>=0&&y<img.height) {
            
            Point p = new Point(x,y);
            int diff = colorDiff(x,y);
            if (!points.contains(p) && !foundPoints.contains(p) && diff<=tolerance) {
              foundPoints.add(p);
            }
            
          }
          
        }
      }
    }
  
  }
  
  return foundPoints;
  
}
