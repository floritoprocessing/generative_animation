import processing.video.*;
import java.awt.Point;

PImage img;

Point lastPoint;
ArrayList lastPoints;
ArrayList points = new ArrayList();
ArrayList gapPoints = new ArrayList();

ArrayList floatyPoints = new ArrayList();
ArrayList floatyLines = new ArrayList();

int nthPoint = 10;
int findR = 255, findG = 255, findB = 255;
int tolerance = 255;

void setup() {
  size(800,600);
  img = loadImage("FunLines.png");
  img.loadPixels();
  
  // get first point by finding "white" from top of picture
  Point p = null;
  boolean found = false;
  int x=0, y=0;
  do {
    found = isLegalImagePoint(x,y);
    if (found) {
      p = new Point(x,y);
    }
    x++;
    if (x==img.width) {
      x=0;
      y++;
      if (y==img.height) found = true;
    }
  } while (!found);
  println(p);
  
  points.add(p);
  lastPoints = new ArrayList();
  lastPoints.add(p);
}

void draw() {
  
  for (int i=0;i<1;i++) {
    ArrayList newPoints = getBestPointsAround(points, lastPoints);
    if (newPoints.size()>0) {
      points.addAll(newPoints);
      for (int j=0;j<points.size();j+=nthPoint) {
        Point gp = (Point)points.get(j);
        if (!gapPoints.contains(gp)) {
          gapPoints.add(gp);
        }
      }
    }
    lastPoints = newPoints;
  }
  // find more points
  //Point newPoint = getBestPointAround(points, lastPoint);
  //println(newPoint);
//  if (newPoint != null) {
//    points.add(newPoint);
//  }
//  lastPoint = newPoint;
  
  background(0);
  image(img,0,0);
  
  strokeWeight(2);
  stroke(128,0,0);
  Point p;
  for (int i=0;i<points.size();i++) {
    p = (Point)points.get(i);
    //point(p.x,p.y);
  }
  
  stroke(255);
  strokeWeight(1);
  noFill();
  for (int i=0;i<gapPoints.size();i++) {
    p = (Point)gapPoints.get(i);
    //ellipse(p.x,p.y,10,10);
  }
  
  // make random floaty points
  for (int i=0;i<8;i++) {
    int randomIndex = (int)random(points.size());
    float dir = random(TWO_PI);
    float spd = Math.random()<0.9 ? 0.5 : 2.0;
    float mx = cos(dir)*spd;
    float my = sin(dir)*spd;
    p = (Point)points.get(randomIndex);
    floatyPoints.add( new FloatyPoint(p,mx,my) );
  }
  
  // make random floatyLines
  if (floatyPoints.size()>=2) {
    for (int i=0;i<10;i++) {
      
      int randomIndex0 = (int)random(points.size());
      int randomIndex1 = randomIndex0;
      do {
        randomIndex0 = (int)random(points.size());
      } while (randomIndex0==randomIndex1);
      
      FloatyPoint p0 = (FloatyPoint)floatyPoints.get(randomIndex0);
      FloatyPoint p1 = (FloatyPoint)floatyPoints.get(randomIndex1);
      float dx = p0.x-p1.x;
      float dy = p0.y-p1.y;
      float dist = dx*dx+dy*dy;
      if (dist<50*50) {
        FloatyLine fl = new FloatyLine(p0,p1);
        if (!floatyLines.contains(fl)) {
          floatyLines.add(fl);
        }
      }
    }
  }
  
  // draw and remove floaty points
  synchronized (floatyPoints) {
    for (int i=floatyPoints.size()-1;i>=0;i--) {
      FloatyPoint fp = (FloatyPoint)floatyPoints.get(i);
      if (fp.alive) {
        fp.draw();
      } else {
        floatyPoints.remove(i);
      }
    }
  }
  
  // draw and remove floaty lines
  println(floatyLines.size());
  for (int i=floatyLines.size()-1;i>=0;i--) {
    FloatyLine fl = (FloatyLine)floatyLines.get(i);
    if (fl.p0.alive || fl.p1.alive) {
      fl.draw();
    } else {
      floatyLines.remove(i);
    }
  }
    //exit();
}
