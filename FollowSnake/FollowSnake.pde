int nrOfDots=150;                          // number of ellipses
int screenW = 600;                         // screen width
int screenH = 300;                         // screen height

int easeA=7;                               // easeA and easeB values for easing
int easeB=easeA+1;
int i;

Dot[] d=new Dot[nrOfDots+1];               // definition of array of objects "Dot"

void setup() {
  size(600,300,P3D);background(255);            // setup screen
  ellipseMode(RADIUS);noFill();    // setup drawmode for ellipses
  for (i=1;i<=nrOfDots;i++) {      
    d[i]=new Dot(i);                        // create new object "Dot"
    d[i].init(screenW,screenH);             // initialize object "Dot"
  }
}

void draw() {                               // update all positions of "Dot"
  background(255);
  for (i=1;i<=nrOfDots;i++) {
    d[i].update();                      
  }
}

void mousePressed() {                       // press mouse to re-initialize all "Dot"s
  easeA=5+int(random(15));
  easeB=easeA+1;
  for (i=1;i<=nrOfDots;i++) {
    d[i].init(screenW,screenH);
  }
}

class Dot {
  float tx,ty,xpos,ypos;    // target xy and position xy
  int nr, nrA, nrB;
  Dot (int n) {             
    nr=n;                   // instance number
  }

  void init(float x, float y) {    // set initial position on screen
    xpos=random(x*2)-x/2;  
    ypos=random(y*2)-y/2;
  }

  void update() {
    if (nr==1) {
      tx=d[nrOfDots].xpos;//mouseX;
      ty=d[nrOfDots].ypos;//mouseY;
    } else {
      tx=d[nr-1].xpos;
      ty=d[nr-1].ypos;
    }

    xpos=(easeA*xpos+tx)/easeB;
    ypos=(easeA*ypos+ty)/easeB;

    ellipse(xpos,ypos,abs(xpos-tx),abs(ypos-ty));
  }
}
