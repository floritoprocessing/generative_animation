class StickGroup extends Vector {
  
  StickGroup() {
    super();
  }
  
  StickGroup(StickGroup sg) {
    super();
    for (int i=0;i<sg.size();i++) add(new Stick((Stick)sg.elementAt(i)));
  }
  
  StickGroup(StickGroup s0, StickGroup s1, float p) {
    super();
    for (int i=0;i<s0.size();i++) add(new Stick((Stick)s0.elementAt(i),(Stick)s1.elementAt(i),p));    
  }
  
  void attachSticks(int child, int parent, int dot) {
    ((Stick)elementAt(child)).position = ((Stick)elementAt(parent)).dot[dot];
  }
  
  void rotateSticks() {
    for (int i=0;i<size();i++)
      ((Stick)elementAt(i)).rotate();
  }
  
  void drawOnImage(GrafImage img) {
    for (int i=0;i<size();i++)
      ((Stick)elementAt(i)).drawOnImage(img);
  }
  
  void showSticks() {
    for (int i=0;i<size();i++)
      ((Stick)elementAt(i)).show();
  }
}
