import Graf.*;
import java.util.Vector;

int MORPH_IMAGES = 300;

PFont font;
Bmp bmp;
StickGroup[] preset = new StickGroup[2];
StickGroup group = new StickGroup();

int MODE_PREPARE_PRESET = 0;
int MODE_MAKE_PRESET = 1;
int MODE_PREPARE_MORPH = 2;
int MODE_MORPH = 3;
int MODE = MODE_PREPARE_PRESET;

int[] DRAW_COUNT = new int[2];

GrafImage drawImage;
int drawCount;
int savedFrame;
int presetNr;

float pmRandom(float a, float b) {
  return ((random(1.0)<0.5)?-1:1)*random(a,b);
}

void setup() {
  size(500,500,P3D);
  font = loadFont("Georgia-Bold-12.vlw");
  textFont(font,12);
  group = new StickGroup();
  preset[0] = new StickGroup();
  preset[1] = new StickGroup();
  savedFrame=0;
  presetNr=0;
  initAll();
}

void initAllFromPresets(float f) {
  group.clear();
  
  group = new StickGroup(preset[0],preset[1],f);
  
  group.attachSticks(1,0,0);
  group.attachSticks(2,1,0);
  group.attachSticks(3,0,1);
  group.attachSticks(4,1,1);
  drawImage = new GrafImage(width,height);
  drawImage.setBlendMode(2); // add
  drawImage.loadPixels();
  for (int i=0;i<drawImage.pixels.length;i++) drawImage.pixels[i] = 0xFF000000;
  drawImage.updatePixels();
  drawCount = 0;
}

void initAll() {
  group.clear();
  for (int i=0;i<5;i++)
    group.add(new Stick(new Vec(width*random(0.4,0.6),height*random(0.4,0.6)),random(120,200),random(1.0),random(TWO_PI),pmRandom(0.01,0.05)));
    //group.add(new Stick(new Vec(width*random(0.4,0.6),height*random(0.4,0.6)),random(100,200),random(1.0),random(TWO_PI),pmRandom(0.01,0.05)));
  group.attachSticks(1,0,0);
  group.attachSticks(2,1,0);
  group.attachSticks(3,0,1);
  group.attachSticks(4,1,1);
  drawImage = new GrafImage(width,height);
  drawImage.loadPixels();
  drawImage.setBlendMode(2); // add
  for (int i=0;i<drawImage.pixels.length;i++) drawImage.pixels[i] = 0xFF000000;
  drawImage.updatePixels();
  drawCount = 0;
}

void savePreset(int pNr) {
  preset[pNr] = new StickGroup(group);
  DRAW_COUNT[pNr] = drawCount;
}

void saveBmp() {
  bmp=new Bmp(width,height,24);
  bmp.capture24Bit();
  bmp.saveAs("output/SticksMorpher_"+nf(savedFrame,3)+".bmp");
  savedFrame++;
}

void keyPressed() {
  if (MODE==MODE_PREPARE_PRESET) {
    MODE=MODE_MAKE_PRESET;
    initAll();
  }
  else if (MODE==MODE_MAKE_PRESET) {
    if (key=='r'||key=='R') initAll();
    if (key=='a'||key=='A') {
      savePreset(presetNr++);
      if (presetNr==1) {
        MODE=MODE_PREPARE_PRESET;
      } 
      else {
        MODE=MODE_PREPARE_MORPH;
      }
    }
  }
  else if (MODE==MODE_PREPARE_MORPH) {
    if (key==' ') initAllFromPresets(0.0);
    MODE = MODE_MORPH;
  }
}

void draw() {
  if (MODE==MODE_PREPARE_PRESET) {

    background(0);
    fill(255);
    text("Preparing preset "+presetNr,10,20);
    text("Hit [R] to restart with new values",10,40);
    text("Hit [A] to accept preset "+presetNr,10,60);

  } 

  else if (MODE==MODE_MAKE_PRESET) {

    drawCycle();

  }

  else if (MODE==MODE_PREPARE_MORPH) {

    background(0);
    fill(255);
    text("Prepare for morph",10,20);
    text("DRAW_COUNT[0]="+DRAW_COUNT[0],10,40);
    text("DRAW_COUNT[1]="+DRAW_COUNT[1],10,60);
    text("MORPH_IMAGES: "+MORPH_IMAGES,10,80);
    text("Hit space to start!",10,120);

  }
  
  else if (MODE==MODE_MORPH) {
    drawCycle();
    float p = (float)savedFrame/MORPH_IMAGES;
    int maxDC = (int)(DRAW_COUNT[0] + p*(DRAW_COUNT[1]-DRAW_COUNT[0]) );
    fill(255);
    if (drawCount<maxDC) {
      text("Render Frame: "+savedFrame+"/"+MORPH_IMAGES,10,20);
      text("Rendering: "+drawCount+"/"+maxDC,10,40);
    }
    if (drawCount>maxDC) {
      saveBmp();
      initAllFromPresets((float)savedFrame/MORPH_IMAGES);
    }
    if (savedFrame>=MORPH_IMAGES) noLoop();
  }
}


void drawCycle() {
  drawImage.updatePixels();
  image(drawImage,0,0);
  for (int i=0;i<500;i++) {
    group.rotateSticks();
    ((Stick)group.elementAt(2)).lineOnImage(drawImage);
    ((Stick)group.elementAt(4)).lineOnImage(drawImage);
    drawCount++;
  }
}
