/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  double X=0,Y=0,Z=0;
  
  Vec() {
  }

  Vec(double _X, double _Y) {
    X=_X;
    Y=_Y;
  }
  
  Vec(double _X, double _Y, double _Z) {
    X=_X;
    Y=_Y;
    Z=_Z;
  }
  
  Vec(Vec _v) {
    X=_v.X; 
    Y=_v.Y; 
    Z=_v.Z;
  }
  
  void set(Vec _v) {
    X=_v.X; 
    Y=_v.Y; 
    Z=_v.Z;
  }
  
  void set(double _X, double _Y, double _Z) {
    X=_X;
    Y=_Y;
    Z=_Z;
  }
  
  double len() {
    return Math.sqrt(X*X+Y*Y+Z*Z);
  }
  
  double lenSQ() {
    return (X*X+Y*Y+Z*Z);
  }
  
  void gravitateTo(Vec _to, double _mgd, double _fFac) {
    double minGravDist2=_mgd*_mgd;
    double dX=_to.X-X;
    double dY=_to.Y-Y;
    double dZ=_to.Z-Z;
    double d2=dX*dX+dY*dY+dZ*dZ;
    if (d2<minGravDist2) {d2=minGravDist2;}
    //double d=Math.sqrt(d2);
    double F=_fFac/d2;
    add(new Vec(dX*F,dY*F,dZ*F));
  }

  void add(Vec _v) {
    X+=_v.X;
    Y+=_v.Y;
    Z+=_v.Z;
  }
  
  void add(double _X, double _Y, double _Z) {
    X+=_X;
    Y+=_Y;
    Z+=_Z;
  }

  void sub(Vec _v) {
    X-=_v.X;
    Y-=_v.Y;
    Z-=_v.Z;
  }

  void mul(double p) {
    X*=p;
    Y*=p;
    Z*=p;
  }
  
  void div(double p) {
    X/=p;
    Y/=p;
    Z/=p;
  }

  void negX() {
    X=-X;
  }

  void negY() {
    Y=-Y;
  }

  void negZ() {
    Z=-Z;
  }

  void neg() {
    negX();
    negY();
    negZ();
  }
  
  void normaliZe() {
    double l=len();
    if (l!=0) { // if not nullvector
      div(l);
    }
  }
  
  Vec getNormaliZed() {
    double l=len();
    if (l!=0) {
      Vec out=new Vec(this);
      out.div(l);
      return out;
    } else {
      return new Vec();
    }
  }
  
  void setLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/len();
    X*=fac;
    Y*=fac;
    Z*=fac;
  }
  
  void toAvLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/((ml+len())/2.0);
    X*=fac;
    Y*=fac;
    Z*=fac;
  }

  void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double Yn=Y*COS-Z*SIN;
    double Zn=Z*COS+Y*SIN;
    Y=Yn;
    Z=Zn;
  }

  void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double Xn=X*COS-Z*SIN; 
    double Zn=Z*COS+X*SIN;
    X=Xn;
    Z=Zn;
  }

  void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double Xn=X*COS-Y*SIN; 
    double Yn=Y*COS+X*SIN;
    X=Xn;
    Y=Yn;
  }

  void rot(double Xrd, double Yrd, double Zrd) {
    rotX(Xrd);
    rotY(Yrd);
    rotZ(Zrd);
  }
  
  boolean isNullVec() {
    if (X==0&&Y==0&&Z==0) {return true;} else {return false;}
  }
}




Vec vecAdd(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.add(b);
  return out;
}

Vec vecSub(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.sub(b);
  return out;
}

Vec vecMul(Vec a, double b) {
  Vec out=new Vec(a);
  out.mul(b);
  return out;
}

Vec vecDiv(Vec a, double b) {
  Vec out=new Vec(a);
  out.div(b);
  return out;
}

double vecLen(Vec a) {
  return a.len();
}

Vec vecRotY(Vec a, double rd) {
  Vec out=new Vec(a);
  out.rotY(rd);
  return out;
}

Vec rndDirVec(double r) {
  Vec out=new Vec(0,0,r);
  out.rotX(random(-PI/2.0,PI));
  out.rotZ(random(0,2*PI));
  return out;
}

Vec rndPlusMinVec(double s) {
  Vec out=new Vec(-s+2*s*Math.random(),-s+2*s*Math.random(),-s+2*s*Math.random());
  return out;
}

void vecSwap(Vec v1, Vec v2) {
  Vec t=new Vec(v1);
  v1.set(v2);
  v2.set(t);
}

/*****************************
 * adapted 3d functions to Vec
 *****************************/

void vecTranslate(Vec v) {
  translate((float)v.X,(float)v.Y,(float)v.Z);
}

void vecVertex(Vec v) {
  vertex((float)v.X,(float)v.Y,(float)v.Z);
}

void vecLine(Vec a, Vec b) {
  line((float)a.X,(float)a.Y,(float)a.Z,(float)b.X,(float)b.Y,(float)b.Z);
}

void vecRect(Vec a, Vec b) {
  rect((float)a.X,(float)a.Y,(float)b.X,(float)b.Y);
}

void vecCamera(Vec eYe, Vec cen, Vec upaXis) {
  camera((float)eYe.X,(float)eYe.Y,(float)eYe.Z,(float)cen.X,(float)cen.Y,(float)cen.Z,(float)upaXis.X,(float)upaXis.Y,(float)upaXis.Z);
}
