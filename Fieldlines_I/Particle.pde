class Particle {

  
  private float FIELDLINE_STEP = 30;
  private float FIELDLINE_DIST_CHECK = 1.0;
  private boolean FIELDLINE_EDGE_SHOW = false;


  private int FIELD_COLOR = 0xFFFFFF;


  private Vec pos;
  private Vec mov;
  private Vec acc;
  private float gravityPotential=0;


  Particle(float _x, float _y) {
    pos = new Vec(_x,_y);
    mov = new Vec();
    acc = new Vec();
  }




  void makeAccelerationFromPlanets(Vector ps) {
    acc.reset();
    for (int i=0;i<ps.size();i++) {
      acc.add(((Planet)ps.elementAt(i)).getGravityVecAt(pos));
    }
    gravityPotential = 400*acc.getLength();
    gravityPotential = (gravityPotential>255)?255:gravityPotential;
  }


  float getGravityPotential() {
    return gravityPotential;
  }

  void makeFieldline(Vector ps, float fieldCheckDegree) {
    // check fieldline edge:
    FIELDLINE_EDGE_SHOW = false;
    for (float rd=0;rd<=PI;rd+=fieldCheckDegree) {
      float dx = FIELDLINE_DIST_CHECK * cos(rd);
      float dy = FIELDLINE_DIST_CHECK * sin(rd);
      Particle p1 = new Particle(pos.getX()+dx,pos.getY()+dy);
      Particle p2 = new Particle(pos.getX()-dx,pos.getY()-dy);
      p1.makeAccelerationFromPlanets(ps);
      p2.makeAccelerationFromPlanets(ps);

      float i=0;
      while (i<255&&!FIELDLINE_EDGE_SHOW) {
        if ((p1.getGravityPotential()<=i&&p2.getGravityPotential()>i) || (p1.getGravityPotential()>i&&p2.getGravityPotential()<=i)) {
          FIELDLINE_EDGE_SHOW = true;
        }
        i+=FIELDLINE_STEP;
      }

    }
  }

  void update(Vector ps) {
    makeAccelerationFromPlanets(ps);
    mov.add(acc);
    pos.add(mov);
  }

  void setColorFromField() {
    colorMode(HSB,255);
    FIELD_COLOR = color(26/360.0*255+5*gravityPotential/255.0,0.8*255,FIELDLINE_STEP*int(gravityPotential/FIELDLINE_STEP));
    if (FIELDLINE_EDGE_SHOW) {
      FIELD_COLOR = color(26,255,255);
    }
  }

  void draw(float pressField, float pressFieldline) {    
    int sx = (int)pos.getX();
    int sy = (int)pos.getY();
    int BG_COL = get(sx,sy);
    float p1=FIELDLINE_EDGE_SHOW?pressFieldline:pressField;
    float p2=1.0-p1;
    int dc=int((FIELD_COLOR>>16&0xFF)*p1+(BG_COL>>16&0xFF)*p2)<<16;
    dc=dc | int((FIELD_COLOR>>8&0xFF)*p1+(BG_COL>>8&0xFF)*p2)<<8;
    dc=dc | int((FIELD_COLOR&0xFF)*p1+(BG_COL&0xFF)*p2);
    set(sx,sy,dc);
  }

  Vec getPos() {
    return pos;
  }

  void setPos(Vec v) {
    pos = new Vec(v);
  }

}
