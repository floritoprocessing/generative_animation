class Planet {
  
  private float MASS = 1.0;
  private Vec pos;
  private Vec mov;
  
  Planet(float _x, float _y, float _xm, float _ym, float _m) {
    pos = new Vec(_x,_y);
    MASS = _m;
    mov = new Vec(_xm,_ym);
  }
  
  Vec getGravityVecAt(Vec partPos) {
    Vec partToMe = new Vec(vecSub(pos,partPos));
    float distance = partToMe.getLength();
    float F = MASS/distance;
    partToMe.normalizeTo(F);
    return partToMe;
  }
  
  void update() {
    pos.add(mov);
    if (pos.getX()<0||pos.getX()>=width) mov.setX(mov.getX()*-1);
    if (pos.getY()<0||pos.getY()>=height) mov.setY(mov.getY()*-1);
  }
  
  void draw() {
    colorMode(RGB,255);
    ellipseMode(CENTER_RADIUS);
    noFill();
    stroke(255,255,255);
    ellipse(pos.getX(),pos.getY(),2*MASS,2*MASS);
  }
  
}
