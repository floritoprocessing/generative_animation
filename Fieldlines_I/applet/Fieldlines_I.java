import processing.core.*; import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; public class Fieldlines_I extends PApplet {
int PLANETS_AMOUNT = 4;
float PLANET_SPEED_DIR_MIN = 0.02f;
float PLANET_SPEED_DIR_MAX = 0.1f;
float PLANET_MASS_MIN = 2.0f;
float PLANET_MASS_MAX = 10.0f;

boolean SAVE_FRAMES = false;
String SAVE_PATH = "c:\\temp\\";


Vector planets;

public void setup() {
  size(200,200,P3D);
  background(0,0,0);
  planets = new Vector();
  for (int i=0;i<PLANETS_AMOUNT;i++) planets.add(new Planet(random(width),random(height),(random(1.0f)<0.5f?-1:1)*random(PLANET_SPEED_DIR_MIN,PLANET_SPEED_DIR_MAX),(random(1.0f)<0.5f?-1:1)*random(PLANET_SPEED_DIR_MIN,PLANET_SPEED_DIR_MAX),random(PLANET_MASS_MIN,PLANET_MASS_MAX)));
}

public void mousePressed() {
  setup();
}

public void draw() {
  for (float x=0;x<width;x+=3) for (float y=0;y<height;y+=3) {
    Particle p = new Particle(x+random(-1.5f,1.5f),y+random(-1.5f,1.5f));
    p.update(planets);
    p.makeFieldline(planets,PI/2.0f);
    p.setColorFromField();
    p.draw(0.8f,0.5f);
  }
  if (SAVE_FRAMES) saveFrame(SAVE_PATH+"Fieldlines_I_####.tga");
  for (int i=0;i<planets.size();i++) ((Planet)planets.elementAt(i)).update();
}

class Particle {

  
  private float FIELDLINE_STEP = 30;
  private float FIELDLINE_DIST_CHECK = 1.0f;
  private boolean FIELDLINE_EDGE_SHOW = false;


  private int FIELD_COLOR = 0xFFFFFF;


  private Vec pos;
  private Vec mov;
  private Vec acc;
  private float gravityPotential=0;


  Particle(float _x, float _y) {
    pos = new Vec(_x,_y);
    mov = new Vec();
    acc = new Vec();
  }




  public void makeAccelerationFromPlanets(Vector ps) {
    acc.reset();
    for (int i=0;i<ps.size();i++) {
      acc.add(((Planet)ps.elementAt(i)).getGravityVecAt(pos));
    }
    gravityPotential = 400*acc.getLength();
    gravityPotential = (gravityPotential>255)?255:gravityPotential;
  }


  public float getGravityPotential() {
    return gravityPotential;
  }

  public void makeFieldline(Vector ps, float fieldCheckDegree) {
    // check fieldline edge:
    FIELDLINE_EDGE_SHOW = false;
    for (float rd=0;rd<=PI;rd+=fieldCheckDegree) {
      float dx = FIELDLINE_DIST_CHECK * cos(rd);
      float dy = FIELDLINE_DIST_CHECK * sin(rd);
      Particle p1 = new Particle(pos.getX()+dx,pos.getY()+dy);
      Particle p2 = new Particle(pos.getX()-dx,pos.getY()-dy);
      p1.makeAccelerationFromPlanets(ps);
      p2.makeAccelerationFromPlanets(ps);

      float i=0;
      while (i<255&&!FIELDLINE_EDGE_SHOW) {
        if ((p1.getGravityPotential()<=i&&p2.getGravityPotential()>i) || (p1.getGravityPotential()>i&&p2.getGravityPotential()<=i)) {
          FIELDLINE_EDGE_SHOW = true;
        }
        i+=FIELDLINE_STEP;
      }

    }
  }

  public void update(Vector ps) {
    makeAccelerationFromPlanets(ps);
    mov.add(acc);
    pos.add(mov);
  }

  public void setColorFromField() {
    colorMode(HSB,255);
    FIELD_COLOR = color(26/360.0f*255+5*gravityPotential/255.0f,0.8f*255,FIELDLINE_STEP*PApplet.parseInt(gravityPotential/FIELDLINE_STEP));
    if (FIELDLINE_EDGE_SHOW) {
      FIELD_COLOR = color(26,255,255);
    }
  }

  public void draw(float pressField, float pressFieldline) {    
    int sx = (int)pos.getX();
    int sy = (int)pos.getY();
    int BG_COL = get(sx,sy);
    float p1=FIELDLINE_EDGE_SHOW?pressFieldline:pressField;
    float p2=1.0f-p1;
    int dc=PApplet.parseInt((FIELD_COLOR>>16&0xFF)*p1+(BG_COL>>16&0xFF)*p2)<<16;
    dc=dc | PApplet.parseInt((FIELD_COLOR>>8&0xFF)*p1+(BG_COL>>8&0xFF)*p2)<<8;
    dc=dc | PApplet.parseInt((FIELD_COLOR&0xFF)*p1+(BG_COL&0xFF)*p2);
    set(sx,sy,dc);
  }

  public Vec getPos() {
    return pos;
  }

  public void setPos(Vec v) {
    pos = new Vec(v);
  }

}

class Planet {
  
  private float MASS = 1.0f;
  private Vec pos;
  private Vec mov;
  
  Planet(float _x, float _y, float _xm, float _ym, float _m) {
    pos = new Vec(_x,_y);
    MASS = _m;
    mov = new Vec(_xm,_ym);
  }
  
  public Vec getGravityVecAt(Vec partPos) {
    Vec partToMe = new Vec(vecSub(pos,partPos));
    float distance = partToMe.getLength();
    float F = MASS/distance;
    partToMe.normalizeTo(F);
    return partToMe;
  }
  
  public void update() {
    pos.add(mov);
    if (pos.getX()<0||pos.getX()>=width) mov.setX(mov.getX()*-1);
    if (pos.getY()<0||pos.getY()>=height) mov.setY(mov.getY()*-1);
  }
  
  public void draw() {
    colorMode(RGB,255);
    ellipseMode(CENTER_RADIUS);
    noFill();
    stroke(255,255,255);
    ellipse(pos.getX(),pos.getY(),2*MASS,2*MASS);
  }
  
}

class Vec {

  private float x=0, y=0, z=0;

  Vec() {
  }

  Vec(float _x, float _y) {
    x = _x;
    y = _y;
  }

  Vec(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
  }
  
  Vec(Vec v) {
    x = v.getX();
    y = v.getY();
    z = v.getZ();
  }

  public float getX() { 
    return x; 
  }
  public float getY() { 
    return y; 
  }
  public float getZ() { 
    return z; 
  }
  
  public float getLength() {
    return sqrt(x*x+y*y+z*z);
  }



  public void reset() {
    x = 0;
    y = 0;
    z = 0;
  }
  public void setX(float _x) { 
    x = _x; 
  }
  public void setY(float _y) { 
    y = _y; 
  }
  public void setZ(float _z) { 
    z = _z; 
  }
  
  public void add(Vec b) {
    x += b.getX();
    y += b.getY();
    z += b.getZ();
  }
  
  public void mul(float m) {
    x *= m;
    y *= m;
    z *= m;
  }
  
  public void normalizeTo(float f) {
    float d = this.getLength();
    if (d!=0) {
      float scaleFac = f/d;
      this.mul(scaleFac);
    }
  }
  

}

public Vec vecSub(Vec a, Vec b) {
  return new Vec(a.getX()-b.getX(),a.getY()-b.getY(),a.getZ()-b.getZ());
}
static public void main(String args[]) {   PApplet.main(new String[] { "Fieldlines_I" });}}