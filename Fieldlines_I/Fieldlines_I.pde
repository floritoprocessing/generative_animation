import java.util.Vector;

int PLANETS_AMOUNT = 4;
float PLANET_SPEED_DIR_MIN = 0.02;
float PLANET_SPEED_DIR_MAX = 0.1;
float PLANET_MASS_MIN = 2.0;
float PLANET_MASS_MAX = 10.0;

boolean SAVE_FRAMES = false;
String SAVE_PATH = "c:\\temp\\";


Vector planets;

void setup() {
  size(200,200,P3D);
  background(0,0,0);
  planets = new Vector();
  for (int i=0;i<PLANETS_AMOUNT;i++) planets.add(new Planet(random(width),random(height),(random(1.0)<0.5?-1:1)*random(PLANET_SPEED_DIR_MIN,PLANET_SPEED_DIR_MAX),(random(1.0)<0.5?-1:1)*random(PLANET_SPEED_DIR_MIN,PLANET_SPEED_DIR_MAX),random(PLANET_MASS_MIN,PLANET_MASS_MAX)));
}

void mousePressed() {
  setup();
}

void draw() {
  for (float x=0;x<width;x+=3) for (float y=0;y<height;y+=3) {
    Particle p = new Particle(x+random(-1.5,1.5),y+random(-1.5,1.5));
    p.update(planets);
    p.makeFieldline(planets,PI/2.0);
    p.setColorFromField();
    p.draw(0.8,0.5);
  }
  if (SAVE_FRAMES) saveFrame(SAVE_PATH+"Fieldlines_I_####.tga");
  for (int i=0;i<planets.size();i++) ((Planet)planets.elementAt(i)).update();
}
