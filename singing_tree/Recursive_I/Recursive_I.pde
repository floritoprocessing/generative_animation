int nr_twigs;
int maxLevel, maxId;
TwigType[] tree;
boolean newTree;

float rotY;

void setup() {
  size(400,300,P3D); background(244,240,255); stroke(30,205,0);

  maxLevel=7; maxId=7;
  tree=new TwigType[40000]; //
  
  rotY=0; newTree=true;
}

void mousePressed() {
  newTree=true;
}

void draw() {
  if (newTree) {nr_twigs=0;twig(0,0,0,50,0,true);newTree=false;}
  background(244,240,255); lights();
  
  translate(200,300);
  rotateY(rotY); rotY+=0.01;
  nr_twigs=0;
  twig(0,0,0,0,0,false);
}



void twig(int id, float dir1, float dir2, float len, int lev, boolean create) {
  int children=0;
  if (create) {
    children=(int)random(1,maxId);
    tree[nr_twigs]=new TwigType(dir1,dir2,len,children);  
  } else {
    children=tree[nr_twigs].children;
    dir1=tree[nr_twigs].dir1; dir2=tree[nr_twigs].dir2;
    len=tree[nr_twigs].len;
  }
  nr_twigs++;
  
  pushMatrix();
  rotateZ(dir1);
  rotateY(dir2);
  line(0,0,0,0,-len,0);
  translate(0,-len,0);
  if (lev<maxLevel) {
    for (int i=0;i<children;i++) {
      twig(i,random(-0.9,0.9),random(TWO_PI),len*random(0.6,0.9),lev+1,create);
    }
  }
  popMatrix();
  
}

class TwigType {
  float dir1,dir2;
  float len;
  int children;
  TwigType(float dir1, float dir2, float len, int children) {
    this.dir1=dir1; this.dir2=dir2; this.len=len; this.children=children;
  }
}
