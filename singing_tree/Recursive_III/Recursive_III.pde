int[] imgbuf; int maxpix;

int nr_twigs;
int maxLevel, maxId; float bBend, shrink1, shrink2;
TwigType[] tree;
boolean newTree;

float rotY;

void setup() {
  size(600,450,P3D); background(244,240,255); //stroke(30,205,0,32);//smooth();
  maxpix=width*height; imgbuf=new int[maxpix];

  maxLevel=7; maxId=5; bBend=55*PI/180; shrink1=0.7; shrink2=0.88;
  tree=new TwigType[100000]; //
  
  rotY=0; newTree=true;
}

void mousePressed() { newTree=true; }
void draw() {
  if (newTree) {
    nr_twigs=0;twig(0,0,0,70,0,random(0.5,2),10*PI/180.0,random(TWO_PI),0.2,true);
    newTree=false;
  }
  background(244,240,255); lights();
  translate(300,380);
  rotateY(rotY); rotY+=0.002;
  nr_twigs=0; twig(0,0,0,0,0,0,0,0,0,false);
}



void twig(int id, float dir1, float dir2, float len, int lev, float frq, float amp, float pha, float damp, boolean create) {
  int children=0;
  if (create) {
    children=(int)random(2,maxId);
    tree[nr_twigs]=new TwigType(dir1,dir2,len,children,frq,amp,pha,damp);  
  } else {
    children=tree[nr_twigs].children;
    dir1=tree[nr_twigs].dir1; dir2=tree[nr_twigs].dir2;
    len=tree[nr_twigs].len;
    frq=tree[nr_twigs].frq; 
    amp=tree[nr_twigs].amp; 
    pha=tree[nr_twigs].pha;
    damp=tree[nr_twigs].damp;
    if (dist(mouseX,mouseY,screenX(0,0,0),screenY(0,0,0))<len) {
      tree[nr_twigs].damp=(50*tree[nr_twigs].damp+1.0)/51.0;
    } else {
      tree[nr_twigs].damp=(30*tree[nr_twigs].damp+0.2)/31.0;
    }
    
    amp*=damp;
  }
  nr_twigs++;
  
  pushMatrix();
  float zrot=dir1+amp*sin(frq*millis()/1000.0+pha);
  rotateZ(zrot);
  rotateY(dir2);
  float p=lev/(float)maxLevel;
  stroke(50-p*20,20+p*185,0,255);//stroke(30,205,0);
  line(0,0,0,0,-len,0);
  translate(0,-len,0);
  if (lev<maxLevel) {
    for (int i=0;i<children;i++) {
      float ratToChild=random(shrink1,shrink2);
      twig(i,random(-bBend,bBend),random(TWO_PI),len*ratToChild,lev+1,frq*ratToChild,amp,pha+random(HALF_PI),damp,create);
    }
  }
  popMatrix();
  
}

class TwigType {
  float dir1,dir2,frq,amp,pha,damp;
  float len;
  int children;
  TwigType(float dir1, float dir2, float len, int children, float frq, float amp, float pha, float damp) {
    this.dir1=dir1; this.dir2=dir2; this.len=len; this.children=children;
    this.frq=frq; this.amp=amp; this.pha=pha;
    this.damp=damp;
  }
}
