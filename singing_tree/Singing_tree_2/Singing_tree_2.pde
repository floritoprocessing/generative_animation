import processing.opengl.*;

import pitaru.sonia_v2_9.*;

//                  maxLevel  maxId         bBend  shrink1  shrink2  maxTwigLen  mainSpeed
float[][] preset={ {       6,     5,  55*PI/180.0,     0.7,    0.88,     70,        1.0 }, 
                   {       7,     4,  70*PI/180.0,     0.8,    0.95,     40,        1.5 },
                   {       4,     6,  95*PI/180.0,     0.8,    0.95,     100,       1.0 },
                   {       8,     4,  45*PI/180.0,     0.5,    0.75,     60,        0.5 },
                   {      12,     3,  35*PI/180.0,     0.8,    0.85,     60,        0.33 } };
                   
void getPreset(int i) {
  maxLevel=int(preset[i][0]); maxId=int(preset[i][1]); 
  bBend=preset[i][2]; shrink1=preset[i][3]; shrink2=preset[i][4];
  maxTwigLen=preset[i][5]; mainSpeed=preset[i][6];
}

// PRESET variables:
int maxLevel=6;          // maximum depth level
int maxId=5;             // maximum number of children
float bBend=55*PI/180.0; // bendangle from one twig to the next
float shrink1=0.7;       // minimum shrink factor to child twig
float shrink2=0.88;      // maximum shrink factor to child twig
float maxTwigLen=70;     // start twig length (used for getting pitch of twig)
float mainSpeed=1.0;     // pitching of all sounds plus growing speed
boolean smoothing=false; // setting for smoothing on/off;

SliderFloat growLevel =  new SliderFloat(0);       // level for growing (related to depth level);
SliderFloat growLevelPercentage = new SliderFloat(0);
Slider fullHeightSlider;

boolean growing=true;     // grow shrink process
boolean shrinking=false; // grow shrink process
TwigType[] tree=new TwigType[100000];  // tree object
int shade=70<<16|70<<8|70;          // shadecolor of tree
int[] shadeBuf=new int[600*450];
int bgCol=244<<16|240<<8|255;

// variables used for main loop:
int nr_twigs;     // total number of twigs (used for stepping through all twigs)
boolean newTree;  // checks if a new tree will be created
float minTwigLen; // minimum twig length (used for getting pitch of twig)
float rotY=0;       // Y-rotation of whole tree
float frameTime,lastMillis; // used for calculating frame time

// sampler:
Sampler Celesta=new Sampler();
//Sample loopMusic;

Slider freqSlider;

PFont font;

void setup() {
  size(600,600,P3D); 
  background(bgCol);
  
  font = loadFont("ArialMT-12.vlw");
  textFont(font,12);
  //smooth();
  //lights();
  
  Sonia.start(this,22050); 
  Celesta.initPolyphony(256); 
  Celesta.initSounds();
  
  //loopMusic=new Sample("Cisloop2.aif"); 
  //loopMusic.setVolume(0);
  frameTime=1/25.0; 
  lastMillis=millis(); 
  newTree=true;
  
  fullHeightSlider = new Slider(growLevelPercentage,0,1,10,460,"Size",font,Slider.VERTICAL);
  freqSlider = new Slider(new SliderFloat(random(0.1,0.5)),0.005,3.5,30,460,"Frequency",font,Slider.VERTICAL);
}

/*
void mousePressed() { 
  shrinking=true; 
  growing=false;
}*/

void keyPressed() {
  if (key=='n') {
    shrinking=true; 
    growing=false;
  }
  if (key=='p') {
    if (shrinking||growing) {
      shrinking=false;
      growing=false;
    } else {
      shrinking=true;
      growing=false;
    }
  }
  
  if (key=='s'||key=='S') {
    smoothing=!smoothing;
    if (smoothing) {smooth();} else {noSmooth();}
  }
}

void draw() {
  
  background(bgCol); 
  
  if (newTree) { 
    newTree=false;
    //background(bgCol);
    int rnd=(int)random(5);
    getPreset(rnd); 
    growLevel.set(0);
    println("preset: "+rnd);
    minTwigLen=maxTwigLen;
    nr_twigs=0;
    twig(0,0,0,maxTwigLen,1,freqSlider.value,10*PI/180.0,random(TWO_PI),true);
    float sp=2*mainSpeed; 
    if (sp>=1) sp/=2;
    
    /*
    loopMusic.stop();
    loopMusic.setVolume(0);
    loopMusic.setSpeed(sp);
    loopMusic.play();
    loopMusic.repeat();
    */
  }
  
  strokeWeight(1);
  
  if (fullHeightSlider.update()) {
    shrinking = false;
    growing = false;
    growLevel.set(fullHeightSlider.value.get()*maxLevel);
  } else {
    fullHeightSlider.value.set(growLevel.get()/maxLevel);
  }
  
  freqSlider.update();
  fullHeightSlider.draw();
  freqSlider.draw();
  
  strokeWeight(3);
  pushMatrix();
  translate(300,380);
  rotateY(rotY); 
  rotY+=0.002;
  nr_twigs=0; 
  twig(0,0,0,0,1,freqSlider.value,0,0,false);
  popMatrix();

/*
  if (frameCount%200==0) {
  translate(width/2,height/2,294);
  //fill(bgCol>>16&0xFF,bgCol>>8&0xFF,bgCol&0xFF,1);
  fill(bgCol,1);
  noStroke();
  rectMode(CENTER);
  rect(0,0,width/4,height/4);
  }
  */
  
  if (growing) {
    growLevel.set(growLevel.get()+frameTime*mainSpeed);
    //loopMusic.setVolume(0.35*growLevel.get()/(float)maxLevel);
    if (growLevel.get()>maxLevel) { 
      growLevel.set(maxLevel); 
      growing=false; 
    }
  }
  
  if (shrinking) {
    growLevel.set(growLevel.get()-frameTime*mainSpeed*4.0);
    //loopMusic.setVolume(0.35*growLevel.get()/(float)maxLevel);
    if (growLevel.get()<0.0) { 
      growLevel.set(0); 
      shrinking=false; 
      newTree=true; 
      growing=true; 
    }
  }
  
  frameTime=0.001*(millis()-lastMillis); lastMillis=millis();
}
