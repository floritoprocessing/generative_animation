class SliderFloat {
  
  float value;
  
  SliderFloat(float value) {
    this.value=value;
  }
  
  float floatValue() {
    return value;
  }
  
  float get() {
    return value;
  }
  
  
  void set(float value) {
    this.value=value;
  }
  
}
