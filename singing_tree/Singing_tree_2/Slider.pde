class Slider {

  SliderFloat value;  
  float vMin,vMax;
  float x,y;
  float width, height;
  String name;
  PFont font=null;
  
  public static final int VERTICAL = 0;
  public static final int HORIZONTAL = 1;
  
  private int type = VERTICAL;
  private float boxCenterValMin, boxCenterValMax, boxCenter, boxSize;
  private boolean over=false;
  private boolean dragging=false;
  private boolean lastMousePressed=false;
  private float ofssetOnMouseDown;
  
  Slider(SliderFloat value,float vMin, float vMax, float x, float y, String name,PFont font, int type) {
    this(value,vMin,vMax,x,y,10,100,name,font);
    this.type=type;
    if (type==HORIZONTAL) {
      float tmp=width;
      this.width=height;
      this.height=tmp;
      setSliderType();
    }
  }
  
  Slider(SliderFloat value,float vMin, float vMax, float x, float y, float width, float height, String name,PFont font) {
    this.value=value;
    this.vMin=vMin;
    this.vMax=vMax;
    this.name=name;
    this.font=font;
    this.x=x;
    this.y=y;
    this.width=width;
    this.height=height;
    
    setSliderType(); 
  }
  
  void setSliderType() {
    // vertical slider:
    if (type==VERTICAL) {
      boxSize = width-2;
      boxCenterValMin = y+height - (width/2.0); //(// height - box width / 2.0 );
      boxCenter = boxCenterValMax = y + (width/2.0);
    } else {
      // horizontal slider:
      boxSize = height-2;
      boxCenterValMin = x + (height/2.0); //(// height - box width / 2.0 );
      boxCenter = boxCenterValMax = x+width - (height/2.0);
    }
  }
  
  public boolean update() {
    
    
    
    // vertical:
    if (type==VERTICAL)
      over = mouseX>x&&mouseX<x+width&&mouseY>boxCenter-boxSize/2&&mouseY<boxCenter+boxSize/2;
    else
      over = mouseY>y&&mouseY<y+height&&mouseX>boxCenter-boxSize/2&&mouseX<boxCenter+boxSize/2;
    
    if (over) {
      // mouseDownEvent:
      if (!lastMousePressed&&mousePressed) {
        dragging=true;
        
        // vertical:
        if (type==VERTICAL)
          ofssetOnMouseDown=mouseY-boxCenter;
        else
          ofssetOnMouseDown=mouseX-boxCenter;
      }
    }
    
    if (dragging) {
      //mouseUpEvent
      if (lastMousePressed&&!mousePressed) {
        dragging=false;
      }
    }
        
    if (!dragging) {
      // value to screen:
      float valuePercentage = (value.floatValue()-vMin)/(vMax-vMin);
      boxCenter = boxCenterValMin + valuePercentage * (boxCenterValMax-boxCenterValMin);
    } else {
      // screen to value:
      // vertical:
      if (type==VERTICAL) {
        boxCenter = mouseY-ofssetOnMouseDown;
        if (boxCenter>boxCenterValMin) boxCenter=boxCenterValMin;
        if (boxCenter<boxCenterValMax) boxCenter=boxCenterValMax;
      } else {
        boxCenter = mouseX-ofssetOnMouseDown;
        if (boxCenter<boxCenterValMin) boxCenter=boxCenterValMin;
        if (boxCenter>boxCenterValMax) boxCenter=boxCenterValMax;        
      }
      float valuePercentage = (boxCenter-boxCenterValMin)/(boxCenterValMax-boxCenterValMin);
      value.set(vMin+valuePercentage*(vMax-vMin));
    }
    
    lastMousePressed = mousePressed;
    
    return dragging;
  }
  
  public void draw() {
    
    rectMode(CORNER);
    noFill();
    stroke(0,0,0);
    rect(x,y,width,height);
    
    
    rectMode(CENTER);
    if (over||dragging) fill(0,0,0,64); else noFill();
    stroke(0);//stroke(over||dragging?255:0,0,0);
    
    // vertical slider:
    if (type==VERTICAL)
      rect(x+width/2,boxCenter,boxSize,boxSize);
    else
      rect(boxCenter,y+height/2,boxSize,boxSize);
    
    if (font!=null) {
      
      fill(0);
      textAlign(LEFT);
      pushMatrix();
      
      // vertical:
      if (type==VERTICAL) {
        translate(x+width+5,y);
        rotate(HALF_PI);
        text(name,0,0); 
      } else {
        translate(x,y-5);
        text(name,0,0);
      }   
      
      popMatrix();
      
      
      // vertical:
      if (type==VERTICAL) {
        textAlign(CENTER);
        text(nf(value.floatValue(),1,3),x+width/2,y+height+10);
      }
      else {
        textAlign(LEFT);
        text(nf(value.floatValue(),1,3),x,y+height+10);
      }
    }
  }
  
}
