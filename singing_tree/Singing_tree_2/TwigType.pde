// Twig data type
// --------------
class TwigType {
  float dir1,dir2;
  SliderFloat frq;
  float frqOff;
  float amp,pha;
  float len;
  int children;
  TwigType(float dir1, float dir2, float len, int children, SliderFloat frq, float amp, float pha) {
    this.dir1=dir1; this.dir2=dir2; this.len=len; this.children=children;
    this.frq=frq; 
    this.frqOff = random(0.9,1.01); 
    this.amp=amp; this.pha=pha;
  }
}



void twig(int id, float dir1, float dir2, float len, int lev, SliderFloat frq, float amp, float pha, boolean create) {
  int children=0;
  if (create) {
    // creation phase:
    if (len<minTwigLen) {minTwigLen=len;}
    children=(int)random(2,maxId);
    tree[nr_twigs]=new TwigType(dir1,dir2,len,children,frq,amp,pha);  
  } else {
    // active phase:
    children=tree[nr_twigs].children;
    dir1=tree[nr_twigs].dir1; dir2=tree[nr_twigs].dir2;
    if (growLevel.get()<lev) {
      float p=constrain(1+growLevel.get()-lev,0,1);
      len=p*tree[nr_twigs].len;
      amp=p*tree[nr_twigs].amp; 
      if (tree[nr_twigs].pha>TWO_PI) {tree[nr_twigs].pha-=TWO_PI;}
    } else {
      len=tree[nr_twigs].len;
      amp=tree[nr_twigs].amp; 
    }
    frq=tree[nr_twigs].frq; 
    tree[nr_twigs].pha+=(frameTime*frq.get()*tree[nr_twigs].frqOff*TWO_PI);
    pha=tree[nr_twigs].pha;
    if (pha>TWO_PI&&growLevel.get()>=lev-1) { 
      tree[nr_twigs].pha-=TWO_PI;
      int i=int(35*(1.0-(tree[nr_twigs].len-minTwigLen)/(maxTwigLen-minTwigLen)));
      float vol=0.7*(len/(float)maxTwigLen)/(float)children;
      float pan=constrain(6*(2*screenX(0,0,0)/(float)width-1.0),-1,1);
      Celesta.playSample(i,vol,pan);
    }
  }
  nr_twigs++;
  
  pushMatrix();
  float sinus=amp*sin(pha); 
  rotateZ(dir1+sinus); 
  rotateY(dir2);
  float p=constrain(1.0-growLevel.get()/(float)maxLevel,lev/(float)maxLevel,1);
  
  stroke(50-p*20,20+p*185,0,64);
  noFill();
  line(0,0,0,0,-len,0);
  
  translate(0,-len,0);

  if (lev<maxLevel) {
    for (int i=0;i<children;i++) {
      float ratToChild=random(shrink1,shrink2);
      twig(i,random(-bBend,bBend),random(TWO_PI),len*ratToChild,lev+1,frq,amp,random(TWO_PI),create);
    }
  }
  popMatrix();
  
}
