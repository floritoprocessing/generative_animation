class Sampler {
  String[] tone={"C","D","Di","F","G","Gi","Ai"}; // minor scale
  Sample[] sample;
  Sample[] polysample; int polyphony,poly_nr;
  
  Sampler() {}
  
  void initPolyphony(int polyphony) {
    polysample=new Sample[polyphony];
    this.polyphony=polyphony;
    poly_nr=0;
  }
  
  void initSounds() {
    sample=new Sample[36];
    for (int i=0;i<36;i++) {
      int okt=3+int(floor(i/7));
      String name="CEL "+tone[i%7]+""+okt+".aif"; //println(name+" | ");
      sample[i]=new Sample(name);
    }
  }
  
  void playSample(int nr, float vol, float pan) {
    polysample[poly_nr]=sample[nr];
    polysample[poly_nr].setVolume(vol);
    polysample[poly_nr].setPan(pan);
    polysample[poly_nr].setSpeed(mainSpeed);
    polysample[poly_nr].play();
    poly_nr++; if (poly_nr==polyphony) {poly_nr=0;}
  }
}
