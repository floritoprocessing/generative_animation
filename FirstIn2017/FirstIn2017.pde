int one = 0xff7CB769;
int two = 0xff183F7C;
  
PGraphics bg;
float[] x, y;
float[] mx, my;
int[] c;

boolean showParticles = true;

void setup() {
  size(960,540,P3D);
  
  bg = createGraphics(width,height,P3D);
  bg.beginDraw();
  bg.background(0);
  bg.endDraw();
  
  x = new float[PARTICLE_AMOUNT];
  y = new float[PARTICLE_AMOUNT];
  mx = new float[PARTICLE_AMOUNT];
  my = new float[PARTICLE_AMOUNT];
  c = new int[PARTICLE_AMOUNT];
  
  for (int i=0;i<PARTICLE_AMOUNT;i++) {
    x[i] = random(width);
    y[i] = random(height);
    mx[i] = random(-1,1);
    my[i] = random(-1,1);
    c[i] = color(255);
  }
}

void keyPressed() {
  if (key=='p') showParticles = !showParticles;
}

void draw() {
  
  bg.beginDraw();
  for (int i=0;i<PARTICLE_AMOUNT;i++) {
    int col = bg.get((int)x[i],(int)y[i]);
    if (col==one) {
      float rd = atan2(my[i],mx[i]);
      rd += radians(1);
      float speed = sqrt(mx[i]*mx[i]+my[i]*my[i]);
      speed *= 0.99f;
      mx[i] = speed * cos(rd);
      my[i] = speed * sin(rd);
    } else {
      if (random(1.0f)<0.02f) {
        float rd = random(TWO_PI);
        float speed = sqrt(mx[i]*mx[i]+my[i]*my[i]);
        speed *= 1.5f;
        mx[i] = speed * cos(rd);
        my[i] = speed * sin(rd);
      }
    }
    
    x[i] += mx[i];
    y[i] += my[i];
    if (x[i]<0) x[i]+=width;
    if (x[i]>=width) x[i]-=width;
    if (y[i]<0) y[i]+=height;
    if (y[i]>=height) y[i]-=height;
  }
  
  
  
  for (int i=0;i<PARTICLE_AMOUNT;i++) {
    if (my[i]>0)
      bg.set((int)x[i],(int)y[i],one);
    else
      bg.set((int)x[i],(int)y[i],two);
  }
  bg.endDraw();
  image(bg,0,0);
  
  if (showParticles)
    for (int i=0;i<PARTICLE_AMOUNT;i++) {
      set((int)x[i],(int)y[i],c[i]);
    }
}