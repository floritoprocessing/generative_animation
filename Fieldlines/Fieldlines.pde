import java.util.Vector;

int PLANETS_AMOUNT = 10;
Vector planets;

void setup() {
  size(200,200,P3D);
  background(0,0,0);
  planets = new Vector();
  for (int i=0;i<PLANETS_AMOUNT;i++) planets.add(new Planet(random(width),random(height),random(1.0,10.0)));
}

void mousePressed() {
  setup();
}

void makeField() {
  for (float x=0;x<width;x+=2) for (float y=0;y<height;y+=2) {
    Particle p = new Particle(x+random(2.0),y+random(2.0));
    p.update(planets);
    p.makeFieldline(planets,2*PI);
    p.draw();
  }
}

void draw() {
  makeField();
  for (int i=0;i<planets.size();i++) ((Planet)planets.elementAt(i)).update();
}
