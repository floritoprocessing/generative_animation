void setup() {
  size(300,500,P3D);
  colorMode(RGB,255); 
  background(0);
  ellipseMode(RADIUS); 
  noStroke(); 
  fill(255,255,255,32);
  initFlakes();
}

void draw() {
  background(0);
  wind.update();
  addFlakes(10);
  updateFlakes();
}





/////////////////////////////////////////////////////////////////////////
////////////////////////////// CLASS FLAKE //////////////////////////////

int nrOfFlakes=10;
int nrOfMaxFlakes=2000;
Flake[] flake;

void initFlakes() {
  flake=new Flake[nrOfMaxFlakes];
  for (int i=0;i<nrOfMaxFlakes;i++) {
    flake[i]=new Flake();
  }
}

void addFlakes(int nr) {
  if (nrOfFlakes+nr<=nrOfMaxFlakes) {
    nrOfFlakes+=nr;
  }
  if (nrOfFlakes%100==0&&nrOfFlakes!=nrOfMaxFlakes) {
    println("flakes: "+nrOfFlakes+"/"+nrOfMaxFlakes);
  }
}

void updateFlakes() {
  for (int i=0;i<nrOfFlakes;i++) {
    flake[i].update();
  }
}

class Flake {
  float base_x,x,y,xmov,ymov;
  float xoff_freq, xoff_depth;
  float trans1 = 2*PI/1000;
  float scl;
  boolean active=true;

  Flake() {
    init();
    y=-random(height);
  }

  void init() {
    x=random(-width,2*width);
    base_x=x;
    y=-10;
    ymov=(1+random(2.5))/1.0;
    xmov=0;
    xoff_freq=0.2+random(0.4);
    xoff_depth=random(20);
    scl=8*pow(random(1),20);
  }

  void update() {
    if (active) {
      y+=ymov;
      xmov=wind.WindFunc(y);
      base_x+=xmov;
      x=base_x + xoff_depth*sin(xoff_freq*trans1*millis());
      if (x<0) x+=width;
      if (x>=width) x-=width;
      ellipse(x,y,scl,scl);
      ellipse(x-width,y,scl,scl);
      ellipse(x+width,y,scl,scl);
      if (y>height+10) {
        init();
      }
    }
  }
}

////////////////////////////// end CLASS FLAKE //////////////////////////////
/////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////
////////////////////////////// CLASS WIND //////////////////////////////

Wind wind=new Wind();

class Wind {
  float w_stren=0;
  float windDir=1;
  boolean changeDir=false;
  int changeDirTo=-1;

  Wind() {
  }

  void update() {
    if (random(300)<1) {
      changeDir=true;
      changeDirTo*=-1;
    }
    if (changeDir) {
      windDir+=(0.01*changeDirTo);
      if (windDir<-1) {
        changeDir=false;
        windDir=-1;
      }
      if (windDir>1) {
        changeDir=false;
        windDir=1;
      }
    }
    w_stren = (7*w_stren + windDir*random(8))/8.0;
  }

  float WindFunc(float y) {
    return (w_stren*(y/2000.0));
  }

}

////////////////////////////// end CLASS WIND //////////////////////////////
////////////////////////////////////////////////////////////////////////////
